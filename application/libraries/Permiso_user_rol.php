<?php if ( ! defined('BASEPATH')) exit('No se permite acceso directo al script'); 
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package CodeIgniter
 * @author  ExpressionEngine Dev Team
 * @copyright  Copyright (c) 2006, EllisLab, Inc.
 * @license http://codeigniter.com/user_guide/license.html
 * @link http://codeigniter.com
 * @since   Version 1.0
 * @filesource
 */

// --------------------------------------------------------------------

/**
 * CodeIgniter Permiso_user_rol Class
 *
 *Esta clase pemite la denegacion de paginas usuarios que no la tengan en sus roles
 *
 * @package		CodeIgniter
 * @author		Darwin Cedeño
 * @subpackage	Libraries
 * @category	Libraries
 * @copyright  Copyright (c) 2014, SATDC.
 * @version 1
 * 
 */
 
class Permiso_user_rol {

	var $CI;
	var $page_valida =FALSE;
	var $roles_user;
	
	 /**
	 * Constructor
	 *
	 * carga la libreria session y obtiene de la variable de session los roles del usuario logueado
	 * default Permiso_user_rol
	 *
	 * @access	public
	 */
	 
	  function Permiso_user_rol()
   {
      // Copy an instance of CI so we can use the entire framework.
      $this->CI =& get_instance();
      $this->CI->load->library('session');
	 $this->roles_user= $this->CI->session->userdata('roles');
	  
   }
   
   
   // --------------------------------------------------------------------
   
   /**
    *valida si el usuario logueado tiene roles validos para la pagina
    *
    * @access  public
    * @param   string  rol para validarlo solo los roles actuales del usuario logueado
    * @return  void
    */

	 public function validar_rol($rol)
 	 {
		 if($this->roles_user===FALSE or $this->CI->session->userdata('is_logued_in')===FALSE or $this->CI->input->cookie('men_corresp',TRUE)===FALSE )
		{
			redirect(base_url().'/login/logout');
		}
		
	 	foreach ($this->roles_user as $rol_user_item):
	 		if($rol_user_item['nombre']==$rol)
			{
				$this->page_valida=TRUE;
			}	 
	 	endforeach;
	
		if($this->page_valida==FALSE)
		{
			show_error('NO TIENE PERMISO PARA VER ESTA PAGINA');
			
		}
 	}
	
}
 /* Fin del archivo Permiso_user_rol.php */
