<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Int_enviada_Model extends CI_Model {

	/**
	* inserta la correspondencia
	* 
	* @param	string
	*
	*/
	
	public function set_int_enviada($name_file,$ip,$corresp_id,$list_con_copia)
	{
		$this->db->close();
		$id_depen_remitente		=	$this->session->userdata('id_area_laboral',TRUE);
		$id_depen_destinatario  =	$this->input->post('list_area_laboral',TRUE);
		$contenido				=	$this->input->post('contenido');
		$asunto					=	$this->input->post('asunto',TRUE);
		$remitente				=	$this->input->post('list_funcionario_remitente',TRUE);
		$destinatario			=	$this->input->post('list_funcionario',TRUE);
		$enviadopor				=	$this->session->userdata('user_login',TRUE);
		$confidencial 			=	$this->input->post('confidencial',TRUE);
		$id_sub_area_laboral	=	$this->input->post('id_sub_area',TRUE);
		$coord_area				=	$this->session->userdata('coord_area',TRUE);
		$coord_subarea			=	$this->session->userdata('coord_sub_area',TRUE);
		//$this->db->trans_start();
	$query = $this->db->query("CALL set_int_enviada(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",array('id_depen_remitente'=>$id_depen_remitente,
																			'id_depen_destinatario'=>$id_depen_destinatario,
																			'contenido'=>$contenido,
																			'asunto'=>$asunto,
																			'remitente'=>$remitente,
																			'destinatario'=>$destinatario,
																			'name_file'=>$name_file,
																			'enviadopor'=>$enviadopor,
																			'ip'=>$ip,
																			'confidencial'=>$confidencial,
																			'corresp_id'=>$corresp_id,
																			'id_sub_area_laboral'=>$id_sub_area_laboral,
																			'list_con_copia'=>$list_con_copia,
																			'coord_area'=>$coord_area,
																			'coord_subarea'=>$coord_subarea));
		return $query->row();

	}
	
	/**
	* Actualiza la correspondencia asignandole el correo del destinatario, en caso de que el correo se haya
	* enviado satisfactoriamente
	* @param	integer
	* @param	string
	*
	*/
	
	public function update_corresp_int_correo($id_corresp_int,$correo_dest,$correo_jefe_dest)
	{
		$this->db->close();
		$query = $this->db->query("CALL update_corresp_int_correo(?,?,?)",array('id_corresp_int'  =>$id_corresp_int,
																			  'correo_dest'	    =>$correo_dest,
																			  'correo_jefe_dest'=>$correo_jefe_dest));
		
	}
	
	public function get_pdf_preview($id_remitente,$id_destinatario,$dest_con_copia)
	{

	  	$query = $this->db->query("CALL get_pdf_preview(?,?,?)",array('id_remitente'    =>$id_remitente,
																	'id_destinatario'  =>$id_destinatario,
																	'dest_con_copia'  =>$dest_con_copia));
	  return $query->result_array();
  

	}
	
	public function get_sub_area_laboral_func($id_funcionario)
	{
		$query = $this->db->query("CALL sub_area_laboral_func(?)",array('id_funcionario'	=>	$id_funcionario));
	  	return $query->result_array();
	}

	
	//update_interna_enviada
		public function update_int_enviada($name_file,$ip,$list_con_copia)
	{
		$this->db->close();
		$id_depen_remitente		=	$this->session->userdata('id_area_laboral',TRUE);
		$id_depen_destinatario  =	$this->input->post('list_area_laboral',TRUE);
		$contenido				=	$this->input->post('contenido');
		$asunto					=	$this->input->post('asunto',TRUE);
		$remitente				=	$this->input->post('list_funcionario_remitente',TRUE);
		$destinatario			=	$this->input->post('list_funcionario',TRUE);
		$enviadopor				=	$this->session->userdata('user_login',TRUE);
		$confidencial 			=	$this->input->post('confidencial',TRUE);
		$id_sub_area_laboral	=	$this->input->post('id_sub_area',TRUE);
		$coord_area				=	$this->session->userdata('coord_area',TRUE);
		$coord_subarea			=	$this->session->userdata('coord_sub_area',TRUE);
		$corresp_id				=	decrypt($this->input->post('id_corresp',TRUE));
		//$this->db->trans_start();
	$query = $this->db->query("CALL update_int_enviada(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",array('id_depen_remitente'=>$id_depen_remitente,
																			'id_depen_destinatario'=>$id_depen_destinatario,
																			'contenido'=>$contenido,
																			'asunto'=>$asunto,
																			'remitente'=>$remitente,
																			'destinatario'=>$destinatario,
																			'name_file'=>$name_file,
																			'enviadopor'=>$enviadopor,
																			'ip'=>$ip,
																			'confidencial'=>$confidencial,
																			'corresp_id'=>$corresp_id,
																			'id_sub_area_laboral'=>$id_sub_area_laboral,
																			'list_con_copia'=>$list_con_copia,
																			'coord_area'=>$coord_area,
																			'coord_subarea'=>$coord_subarea));
		return $query->row();

	}
	
	public function status_corresp_env($corresp_id,$id_status,$text)
	{

	  	$query = $this->db->query("CALL update_status_corresp_env(?,?,?)",array('corresp_id' =>$corresp_id,
																			  'id_status'  =>$id_status,
																			  'text'  =>$text));
	  return $query->result_array();
  

	}
	
	
}

/* End of file int_enviada_model.php */
/* Location: .application/models/int_enviada/int_enviada_model.php */