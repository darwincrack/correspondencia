<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Ext_enviada_Model extends CI_Model {


	
	/**
	* inserta la correspondencia
	* 
	* @param	string
	*
	*/
	
	public function set_ext_enviada($ip)
	{
		$id_depen_remitente		=	$this->session->userdata('id_area_laboral');
		$remitente				=	$this->input->post('list_funcionario_remitente');
		$id_depen_destinatario  =	$this->input->post('depen_destinatario');		
		$destinatario			=	$this->input->post('destinatario');		
		$contenido				=	$this->input->post('contenido');
		$asunto					=	$this->input->post('asunto');
		$enviadopor				=	$this->session->userdata('user_login');
		$ubic_recep 			=	$this->input->post('ubic_recep');
		
		$query = $this->db->query("CALL set_ext_enviada(?,?,?,?,?,?,?,?,?)",array('id_depen_remitente'=>$id_depen_remitente,
																			'remitente'=>$remitente,
																			'id_depen_destinatario'=>$id_depen_destinatario,
																			'destinatario'=>$destinatario,
																			'contenido'=>$contenido,
																			'asunto'=>$asunto,
																			'enviadopor'=>$enviadopor,
																			'ip'=>$ip,
																			'ubic_recep'=>$ubic_recep));
		
		return $query->row();

	}
	
	public function set_update_corresp_ext_correlativo($id_corresp_ext,$num_correlativo)
	{
		$user_salida	=	$this->session->userdata('user_login');
		$query = $this->db->query("CALL update_corresp_ext_correlativo(?,?,?)",array('id_corresp_ext'=>$id_corresp_ext,
																					'num_correlativo'=>$num_correlativo,
																					'user_salida'=>$user_salida));
		return $query->row();
	}
	

}

/* End of file ext_enviada_model.php */
/* Location: .application/models/ext_enviada/ext_enviada_model.php */