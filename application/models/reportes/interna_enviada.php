<?php 
class Interna_enviada extends CI_Model {

  public function get_rpt_corresp_env_destinatario()
  {
	  $this->db->close();
	   $fecha_inicial			=	$this->input->post('datepicker');
	   $fecha_final				=	$this->input->post('datepicker2');
   	   $id_depen_destinatario	=	$this->input->post('list_area_laboral');
	   $list_sub_area_laboral	=	$this->input->post('list_sub_area_laboral');
	   $id_depen_remitente		=	$this->session->userdata('id_area_laboral');
$query = $this->db->query("CALL rpt_corresp_env_destinatario(?,?,?,?)",array('id_area_remitente'	=>$id_depen_remitente,
	   																			'id_depen_destinatario'	=>$id_depen_destinatario,
																				'fecha_inicial'			=>$fecha_inicial,
	   																			'fecha_final'			=>$fecha_final));		
																				
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	   
  }
  
  
  
  
    public function get_rpt_corresp_env_destinatario_user()
  	{
	   $this->db->close();
	   $fecha_inicial			=	$this->input->post('datepicker');
	   $fecha_final				=	$this->input->post('datepicker2');
   	   $id_func_destinatario	=	$this->input->post('lista_func_destinatario');
	   $id_depen_remitente		=	$this->session->userdata('id_area_laboral');
	   
		$query = $this->db->query("CALL rpt_corresp_env_destinatario_user(?,?,?,?)",
		array('id_area_remitente'		=>	$id_depen_remitente,
	   		  'id_func_destinatario'	=>	$id_func_destinatario,
			  'fecha_inicial'			=>	$fecha_inicial,
	   		  'fecha_final'				=>	$fecha_final));		
																				
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	   
  	}
  
  public function get_rpt_corresp_env_id($id_corresp_int_env)
  {
  		$this->db->close();
		//$id_corresp_int_env=$this->encrypt->decode($id_corresp_int_env);
		$id_depen_remitente		=	$this->session->userdata('id_area_laboral');
		
		$query = $this->db->query("CALL rpt_corresp_env_id(?,?)",array('id_corresp_int_env'	=>	$id_corresp_int_env
																	 ,'id_depen_remitente'	=>	$id_depen_remitente));
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
  }
  
  
  public function get_rpt_corresp_env_status()
  {
	   $id_depen_remitente		=	$this->session->userdata('id_area_laboral');
$query = $this->db->query("CALL rpt_corresp_env_status(?)",array('id_area_remitente' => $id_depen_remitente));		
																				
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	   
  }
  
  public function get_rpt_corresp_env_modif_id($id_corresp_int_env)
  {
  		$this->db->close();
		//$id_corresp_int_env=$this->encrypt->decode($id_corresp_int_env);
		$id_depen_remitente		=	$this->session->userdata('id_area_laboral');
		
		$query = $this->db->query("CALL rpt_corresp_env_modif_id(?,?)",array('id_corresp_int_env'	=>	$id_corresp_int_env
																	 ,'id_depen_remitente'	=>	$id_depen_remitente));
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
  }
  
  
  
  
    public function get_correo_corresp_env_id($id_corresp_int_env)
  {
	$this->db->close();
		$query = $this->db->query("CALL get_correo_corresp_env_id(?)",array('id_corresp_int_env'	=>	$id_corresp_int_env));
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
  }
  
      public function get_correo_por_aprobar_env_id($id_corresp_int_env)
  {
	$this->db->close();
		$query = $this->db->query("CALL get_correo_por_aprobar_env_id(?)",array('id_corresp_int_env'	=>	$id_corresp_int_env));
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
  }
  
  
 
}

/* End of file interna_enviada.php */
/* Location: .application/models/reportes/interna_enviada.php */