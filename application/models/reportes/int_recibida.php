<?php 
class Int_recibida extends CI_Model {

	function get_rpt_corresp_int_recib_teen()
	{
		$this->db->close();
		$id_depen_destinatario		=	$this->session->userdata('id_area_laboral');
		$funci_id_logueado			=	$this->session->userdata('funcionario_id');
		$coord_area					=	$this->session->userdata('coord_area');
		
$query = $this->db->query("CALL rpt_corresp_int_recib_teen(?,?,?)",array('id_depen_destinatario'=>$id_depen_destinatario,
																	'funci_id_logueado'=>$funci_id_logueado,
																	'coord_area'=>$coord_area));		
																				
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	
	}
	
	
	  public function get_rpt_corresp_recib_id($id_corresp_int_env)
  	  {
  	  		$this->db->close();
			//$id_corresp_int_env=$this->encrypt->decode($id_corresp_int_env);
			$id_depen_destinatario		=	$this->session->userdata('id_area_laboral');
		
			$query = $this->db->query("CALL rpt_corresp_recib_id(?,?)",array('id_corresp_int_env'	=>	$id_corresp_int_env
																	 ,'id_depen_destinatario'	=>	$id_depen_destinatario));
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
  	}
	
	
	
	
	  public function get_rpt_corresp_recib_remitente()
      {
	  	$this->db->close();
	   	$fecha_inicial_envio		=	$this->input->post('datepicker');
	   	$fecha_final_envio			=	$this->input->post('datepicker2');
   	   	$id_depen_remitente			=	$this->input->post('list_area_remitente');
   	   	$list_sub_area_laboral		=	$this->input->post('list_sub_area_laboral');
		
	   	$id_depen_destinatario		=	$this->session->userdata('id_area_laboral');
		$funci_id_logueado			=	$this->session->userdata('funcionario_id');
		$coord_area					=	$this->session->userdata('coord_area');
$query = $this->db->query("CALL rpt_corresp_recib_remitente(?,?,?,?,?,?,?)",array('id_area_remitente'=>$id_depen_remitente,
	   																'id_depen_destinatario'		=>$id_depen_destinatario,
																	'fecha_inicial_envio'		=>$fecha_inicial_envio,
	   																'fecha_final_envio'			=>$fecha_final_envio,
																	'funci_id_logueado'			=>$funci_id_logueado,
																	'coord_area'				=>$coord_area,
																	'list_sub_area_laboral'		=>$list_sub_area_laboral));		
																				
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	   
  	}
	
	
	
	
	//
    public function get_rpt_corresp_env_remitente_user()
  	{
	   $this->db->close();
	   
	   $id_depen_destinatario	=	$this->session->userdata('id_area_laboral');
	   $id_func_remitente		=	$this->input->post('lista_func_remitente');
	   $fecha_inicial			=	$this->input->post('datepicker');
	   $fecha_final				=	$this->input->post('datepicker2');
   	   $funci_id_logueado		=	$this->session->userdata('funcionario_id');
	   $coord_area				=	$this->session->userdata('coord_area');
	   
	   
	   $query = $this->db->query("CALL rpt_corresp_env_remitente_user(?,?,?,?,?,?)",
	   array( 'id_area_remitente'		=>	$id_depen_destinatario,
	   		  'id_func_destinatario'	=>	$id_func_remitente,
			  'fecha_inicial'			=>	$fecha_inicial,
	   		  'fecha_final'				=>	$fecha_final,
			  'funci_id_logueado'		=>	$funci_id_logueado,
			  'coord_area'				=>	$coord_area));		
																				
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	   
  	}
	
}

/* End of file int_recibida.php */
/* Location: .application/models/reportes/int_recibida.php */

