<?php 
class Int_general extends CI_Model {

	public function get_rpt_corresp_int_env_gen()
	{
	    $this->db->close();
	   	$fecha_inicial_envio		=	$this->input->post('datepicker');
	   	$fecha_final_envio			=	$this->input->post('datepicker2');
		$depen_remitente			=	$this->input->post('list_area_laboral');
		
		$query = $this->db->query("CALL rpt_corresp_int_env_gen(?,?,?)",array('depen_remitente'	 =>$depen_remitente,
																			'fecha_inicial_envio'=>$fecha_inicial_envio,
	   																		'fecha_final_envio'  =>$fecha_final_envio));
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	
	}
	
	
	public function get_rpt_corresp_env_remitente_user_gen()
	{
	    $this->db->close();
	   	$list_funcionario		=	$this->input->post('list_funcionario');

		
		$query = $this->db->query("CALL rpt_corresp_env_remitente_user_gen(?)",array('list_funcionario'=>$list_funcionario));
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	
	}	
	
	//
		public function get_rpt_corresp_int_recib_gen()
	{
	    $this->db->close();
	   	$fecha_inicial_envio		=	$this->input->post('datepicker');
	   	$fecha_final_envio			=	$this->input->post('datepicker2');
		$depen_destinatario			=	$this->input->post('list_area_laboral');
		
		$query = $this->db->query("CALL rpt_corresp_int_recib_gen(?,?,?)",array('depen_remitente' =>$depen_destinatario,
																			'fecha_inicial_envio'=>$fecha_inicial_envio,
	   																		'fecha_final_envio'  =>$fecha_final_envio));
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	
	}
	
	public function get_rpt_corresp_int_gen_id($id_corresp_int)
	{
	    $this->db->close();
		$query = $this->db->query("CALL rpt_corresp_int_gen_id(?)",array('id_corresp_int'=>$id_corresp_int));
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	
	}
	
	
	public function get_rpt_corresp_int_num_corresp()
	{
		$id_depend_remit 	=	$this->input->post('list_area_laboral');
		$periodo			=	$this->input->post('periodo');
		$num_corresp		=	$this->input->post('num_corresp');
		$this->db->close();
		$query = $this->db->query("CALL rpt_corresp_int_num_corresp(?,?,?)",array('id_depend_remit'=>$id_depend_remit
																				 ,'periodo'=>$periodo
																				 ,'num_corresp'=>$num_corresp));
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
		
	}
	
	
	public function get_rpt_corresp_int_num_corresp_sub_area()
	{
		$id_depend_remit 			=	$this->input->post('list_area_laboral');
		$id_depend_remit_sub_area 	=	$this->input->post('list_sub_area_laboral');
		$periodo					=	$this->input->post('periodo');
		$num_corresp				=	$this->input->post('num_corresp');
		$this->db->close();
		$query = $this->db->query("CALL rpt_corresp_int_num_corresp_sub_area(?,?,?,?)",array('id_depend_remit'=>$id_depend_remit
																				 ,'periodo'=>$periodo
																				 ,'num_corresp'=>$num_corresp
																				 ,'id_depend_remit_sub_area'=>$id_depend_remit_sub_area));
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
		
	}

}

/* End of file int_general.php */
/* Location: .application/models/reportes/int_general.php */