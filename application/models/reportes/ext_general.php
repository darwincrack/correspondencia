<?php 
class Ext_general extends CI_Model {

	public function get_rpt_corresp_ext_env_gen()
	{
	    $this->db->close();
	   	$fecha_inicial_envio		=	$this->input->post('datepicker');
	   	$fecha_final_envio			=	$this->input->post('datepicker2');
		$depen_remitente			=	$this->input->post('list_area_laboral');
		
		$query = $this->db->query("CALL rpt_corresp_ext_env_gen(?,?,?)",array('depen_remitente'	 =>$depen_remitente,
																			'fecha_inicial_envio'=>$fecha_inicial_envio,
	   																		'fecha_final_envio'  =>$fecha_final_envio));
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	
	}
	
	public function get_rpt_corresp_ext_gen_id($id_corresp_ext)
	{
	    $this->db->close();
		$query = $this->db->query("CALL rpt_corresp_ext_gen_id(?)",array('id_corresp_ext'=>$id_corresp_ext));
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	
	}
	
	
		function get_rpt_corresp_ext_env_dest_gen()
	{
		$this->db->close();
		$ente_personas_dest	=	$this->input->post('ente_persona_dest');
		
		$query = $this->db->query("CALL rpt_corresp_ext_env_dest_gen(?)",array('ente_personas_dest'=>$ente_personas_dest));		
																				
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	
	}
	
	
}
/* End of file ext_general.php */
/* Location: .application/models/reportes/ext_general.php */