<?php  
/**
 * 
 */
class Externa_enviada extends CI_Model {


	function get_rpt_corresp_ext_enviada_recep_teen()
	{
		$this->db->close();
		$id_area_laboral		=	$this->session->userdata('id_area_laboral');
		
		$query = $this->db->query("CALL rpt_corresp_ext_enviada_recep_teen(?)",array('id_area_laboral'=>$id_area_laboral));		
																				
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	
	}
	
	
	function get_rpt_corresp_ext_enviada_recep_fecha()
	{
		$this->db->close();
		$id_area_laboral		=	$this->session->userdata('id_area_laboral');
		$fecha_inicial_envio	=	$this->input->post('datepicker');
	   	$fecha_final_envio		=	$this->input->post('datepicker2');
		
		$query = $this->db->query("CALL rpt_corresp_ext_enviada_recep_fecha(?,?,?)",array('id_area_laboral'=>$id_area_laboral,
																				 	 'fecha_inicial_envio'=>$fecha_inicial_envio,
																					 'fecha_final_envio'=>$fecha_final_envio));		
																				
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	
	}
	
	
	function get_rpt_corresp_ext_enviada_recep_id($id_corresp)
	{
		$this->db->close();
		$id_area_laboral		=	$this->session->userdata('id_area_laboral');

		
		$query = $this->db->query("CALL rpt_corresp_ext_enviada_recep_id(?,?)",array('id_corresp'=>$id_corresp,
																			   'id_area_laboral'=>$id_area_laboral));		
																				
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	
	}
	
	function get_rpt_corresp_ext_enviada_id($id_corresp)
	{
		$this->db->close();
		$id_area_laboral		=	$this->session->userdata('id_area_laboral');

		
		$query = $this->db->query("CALL rpt_corresp_ext_enviada_id(?,?)",array('id_corresp'=>$id_corresp,
																			   'id_area_laboral'=>$id_area_laboral));		
																				
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	
	}
	
	function get_rpt_corresp_ext_env_fecha()
	{
		$this->db->close();
		$id_area_laboral		=	$this->session->userdata('id_area_laboral');
		$fecha_inicial_envio	=	$this->input->post('datepicker');
	   	$fecha_final_envio		=	$this->input->post('datepicker2');
		
		$query = $this->db->query("CALL rpt_corresp_ext_env_fecha(?,?,?)",array('id_area_laboral'=>$id_area_laboral,
																			'fecha_inicial_envio'=>$fecha_inicial_envio,
																			'fecha_final_envio'	=>$fecha_final_envio));		
																				
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	
	}


	function get_rpt_corresp_ext_env_dest()
	{
		$this->db->close();
		$id_area_laboral	=	$this->session->userdata('id_area_laboral');
		$ente_personas_dest	=	$this->input->post('ente_persona_dest');
		
		$query = $this->db->query("CALL rpt_corresp_ext_env_dest(?,?)",array('id_area_laboral'=>$id_area_laboral,
																			'ente_personas_dest'=>$ente_personas_dest));		
																				
		if($query->num_rows()>0)
		{																	
	   		return $query->result_array();
		}
	
	}
}

/* End of file externa_enviada.php */
/* Location: .application/models/reportes/externa_enviada.php */