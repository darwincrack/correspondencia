<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Ext_recibida_Model extends CI_Model {


	public function set_ext_recibida($ip)
	{
		$area_recibido			=	$this->session->userdata('id_area_laboral');
		$depen_remitente		=	$this->input->post('depen_remitente');
		$remitente				=	$this->input->post('remitente');
		$id_depen_destinatario  =	$this->input->post('list_area_laboral');		
		$destinatario_id		=	$this->input->post('list_funcionario');			
		$asunto					=	$this->input->post('asunto');		
		$fecha_corresp			=	$this->input->post('datepicker');
		$num_corresp 			=	$this->input->post('num_corresp');
		$receptor				=	$this->session->userdata('user_login');
		$area_recibido			=	$this->session->userdata('id_area_laboral');

		
		$query = $this->db->query("CALL set_ext_recibida(?,?,?,?,?,?,?,?,?,?)",array('depen_remitente'=>$depen_remitente,
																			'remitente'=>$remitente,
																			'num_corresp'=>$num_corresp,
																			'asunto'=>$asunto,
																			'id_depen_destinatario'=>$id_depen_destinatario,
																			'destinatario_id'=>$destinatario_id,
																			'fecha_corresp'=>$fecha_corresp,
																			'receptor'=>$receptor,
																			'area_recibido'=>$area_recibido,
																			'ip'=>$ip));
		
		return $query->row();

	}

}

/* End of file ext_recibida_model.php */
/* Location: .application/models/ext_recibida/ext_recibida_model.php */