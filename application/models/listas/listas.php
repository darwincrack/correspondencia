<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Listas extends CI_Model {

  public function get_list_area_laboral()
  {
	  $this->db->close();
	  $query = $this->db->query("CALL lista_area_laboral()");
	  return $query->result_array();
  
  }
  
    public function get_lista_recepcion()
  {
	  $this->db->close();
	  $query = $this->db->query("CALL lista_recepcion()");
	  return $query->result_array();
  
  }
  
  public function get_lista_funcionarios($id_area_laboral)
  {
	  $this->db->close();
   	   $query = $this->db->query("CALL lista_funcionarios(?)",array('id_area_laboral'=>$id_area_laboral));
	    return $query->result_array();
	   
  }
  
   public function get_lista_func_destinatario()
  { 
	 $this->db->close();
	 $id_depen_remitente		=	$this->session->userdata('id_area_laboral');
	 $query = $this->db->query("CALL lista_func_destinatario(?)",array('id_depen_remitente'=>$id_depen_remitente));
	  return $query->result_array();
  }
  
  ////
     public function get_lista_func_remitente()
  { 
	 $this->db->close();
	 $id_depen_destinatario		=	$this->session->userdata('id_area_laboral');
	 $query = $this->db->query("CALL lista_func_remitente(?)",array('id_depen_destinatario'=>$id_depen_destinatario));
	  return $query->result_array();
  }
  
  
  
  public function get_ruta_file()
  {
	$this->db->close();
  	$id_area_laboral= $this->input->post('list_area_laboral');
	$query = $this->db->query("CALL ruta_file(?)",array('id_area_laboral'=>$id_area_laboral));
	if($query->num_rows() == 1)
		{
			return $query->row();
		}
  }
  
  	// autocomplete empresas,respuesta en json
    public function get_entes_externos($match)
  	{
		
		$this->db->close();
		$j = 0;
		$query = $this->db->query("CALL entes_externos(?)",array('name_ente'=>$match));
		$results= $query->result();

		foreach($results as $result)
		{
  			$records[$j] = array('id' 	=> $result->id_ente,
					   			'label' => $result->name_ente,
					   			'value' => $result->name_ente
                     			);
 			$j++;				   
		}
		return json_encode(@$records);
  }
 
 
   	// autocomplete personas externas,respuesta en json
    public function get_personas_externas($match,$ente_externo)
  	{
		
		$this->db->close();
		$j = 0;
		$query = $this->db->query("CALL personas_externas(?,?)",array('name_persona'=> $match,
																	  'ente_externo'=> $ente_externo));
		$results= $query->result();
		foreach($results as $result)
		{
  			$records[$j] = array('id' 	=> $result->id_persona,
					   			'label' => $result->name_persona,
					   			'value' => $result->name_persona
                     			);
 			$j++;				   
		}
		return json_encode(@$records);
  }
  
  //obtener sub area laboral
    public function get_lista_sub_area_laboral($id_area_laboral)
  {
   	   $query = $this->db->query("CALL lista_sub_area_laboral(?)",array('id_area_laboral'=>$id_area_laboral));
	    return $query->result_array();
	   
  }
  
  
  //lista de estatus
   public function get_lista_status()
  {
	  $this->db->close();
	  $query = $this->db->query("CALL lista_status()");
	  return $query->result_array();
  
  }
  
  
     function get_status_corresp($corresp_id)
  {
	  	$this->db->close();
   	   $query = $this->db->query("CALL get_status_corresp(?)",array('corresp_id'=>$corresp_id));
	   return $query->row();
  }
  
  
}

/* End of file listas.php */
/* Location: .application/models/listas/listas.php */