<?php  //if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Login_user extends CI_Model {
	

	public function get_login_user()
	{
		$user_name=$this->input->post('usuario',TRUE);
		$pass_user=$this->input->post('contrasena',TRUE);		
		$ip=$this->input->ip_address();
		
		$query = $this->db->query("CALL login(?,?,?)",array('name_user'=>$user_name,'pass_user'=>$pass_user,'ip'=>$ip));
		if($query->num_rows() == 1)
		{
			return $query->row();
		}else{
			$this->session->set_flashdata('usuario_incorrecto','Usuario y/o Contraseña Invalidos');
			redirect(base_url().'login/login','refresh');
		}
		
	}
	
	public function get_rol_user()
	{
		$this->db->close();
		$id_usuario=$this->session->userdata('id_usuario');
		$query = $this->db->query("CALL rol_user(?)",array('id_usuario'=>$id_usuario));
		return $query->result_array();
		
	}
	
}

/* End of file login_user.php */
/* Location: .application/models/login/login_user.php */