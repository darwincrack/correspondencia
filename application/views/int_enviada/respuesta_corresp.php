<div class="formulario">
<?php echo $error_db;?>
<?php echo form_open_multipart("int_enviada/int_enviada/redactar/1/1/$resp_id_corresp/$resp_list_funcionario/$resp_list_area_laboral/$resp_num_corresp/$n_remit_cargo") ?>

    <ul>
       <li>
        
       <label for="depen_destinatario">Seleccione Remitente:</label>
        <?php echo form_error('list_funcionario_remitente'); ?>
       <select name="list_funcionario_remitente" id="list_funcionario_remitente">
	  <?php foreach ($list_funcionario as $item_list_funcionario):?>
	  <option value="<?php echo $item_list_funcionario['funcionario_id']?>" 
	  <?php if($item_list_funcionario['funcionario_id']==$this->session->userdata('funcionario_id')){ echo "selected='selected'";} ?>>
	  <?php echo $item_list_funcionario['nombre_funcionario']?> </option>
	  <?php endforeach;?>
      </select>
       </li>
           <div id="add_sub_area" style="margin-left:15px;">
       	<?php foreach ($list_funcionario as $item_list_funcionario):
       			if($item_list_funcionario['funcionario_id']==$this->session->userdata('funcionario_id') && $item_list_funcionario['id_sub_area_laboral']!="")
				{ 
					echo "  <label>Deseas incluir tu subarea</label> (<b>".$item_list_funcionario['nombre_sub_area_laboral']."-".$item_list_funcionario['iniciales_sub_area_laboral']."</b>) <div style='display:inline'><input type='checkbox' name='id_sub_area' value='".$item_list_funcionario['id_sub_area_laboral']."'>
	<a class='tooltip'><img class='tooltips' src='". site_url('img/info_small.png'). "' />
         <span>Son las Unidades Internas de Cada Coordinacion. La correspondencia sandra con esa numeracion.</span></a>
         </div>";
				}
	    	endforeach;?>
       </div>
       <li><label for="destinatario">destinatario:</label><?php echo $n_remit_cargo?>
       
       <div id="text_copia" style="display:inline;">
       <a style="text-decoration: underline;color: red; cursor: pointer; margin-left:29px" title="Con Copia" id="prueba">CC</a>
       </div>
       </li>
       <div id="content_con_copia"></div>
       
       
       
       <li>
           <label for="asunto">Asunto:</label>
           <?php echo form_error('asunto'); ?>
           <input type="text" name="asunto"  id="asunto" placeholder="Asunto" maxlength="250" value="<?php if(@$valido!=1) echo set_value('asunto'); ?> " autocomplete="off" />
       </li>
		  <li>
           <label for="adjuntar_archivo">Adjuntar Archivo:</label>
           <?php echo $error;?>
           <input type="file" name="userfile" title="extensiones permitidas: doc,docx,xls,xlsx,ppt,pptx,pdf,jpg,gif,rar" id="userfile" style="padding-right:7px;"/>
           <a class="tooltip"><img class="tooltips" src="<?php echo site_url('img/info_small.png') ?>" /><span>*formato:doc,docx,xls,xlsx,ppt,pdf,rar. *Sin Caracteres Especiales.</span></a>
       </li>
       <li>
       	<label for="check_confidencial">Confidencial:</label>
        <input type="checkbox" name="confidencial" value="1" title="Solo podra verlo el usuario Destinatario, ningun otro del departamento de dicho usuario tendra acceso a esta correspondencia">
         <a class="tooltip"><img class="tooltips" src="<?php echo site_url('img/info_small.png') ?>" />
         <span>Solo podra verlo el Funcionario Destinatario, nadie mas perteneciente a ese departamento tendra acceso.</span></a>
       </li>
       <hr style="width: 900px; margin-left: -147px;">
       <br>
       <div style="margin-left: -100px;">
       <?php echo form_error('contenido'); ?>
                 <textarea  name="contenido" id="text_obser"><?php if($valido!=1) echo @$_POST["contenido"]; ?></textarea>
        <span id="tx_char_rest" style="font-size:10px;">Copiar= Ctrl+c; Pegar=Ctrl+v</span>
        </div>
        <li style="text-align:center">
        
           <input type="button" name="vista_previa" id="btn_vista_previa" value="Vista Previa" style="margin: 0;
width: 100px;"><input type="submit" value="Enviar" style="margin:10px"><div id="loading" style="display:none;">cargando...</div>

       </li>
    </ul>
     <?php //cuando es una respuesta de correspondencia
		  echo "
		   <input type='hidden' name='respuesta' id='respuesta' value='1'>
		   <input type='hidden' name='id_corresp' value='$resp_id_corresp'>
		   <input type='hidden' name='list_funcionario' id='list_funcionario' value='$resp_list_funcionario'>
		   <input type='hidden' name='list_area_laboral'  value='".decrypt($resp_list_area_laboral)."'>
		   
		   
		   <input type='hidden' name='num_corresp'  value='$resp_num_corresp'>
		   <input type='hidden' id='location' value='$resp_location' >
		   <input type='hidden' name='n_remit_cargo' value='".encrypt($n_remit_cargo)."'>
		   ";?>
</form>
</div>
