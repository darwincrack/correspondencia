<div class="formulario">
<?php echo $error_db;?>
<?php echo form_open_multipart('int_enviada/int_enviada/modificar/'.$id_corresp.'/'.$location.'/3') ?>

    <ul>
       <li>
        
       <label for="depen_destinatario">Seleccione Remitente:</label>
        <?php echo form_error('list_funcionario_remitente'); ?>
       <select name="list_funcionario_remitente" id="list_funcionario_remitente">
	  <?php foreach ($list_funcionario as $item_list_funcionario):?>
	  <option value="<?php echo $item_list_funcionario['funcionario_id']?>" 
	  <?php if($item_list_funcionario['nombre_funcionario']==$name_remitente){ echo "selected='selected'";} ?>
      >
	  <?php echo $item_list_funcionario['nombre_funcionario']?> </option>
	  <?php endforeach;?>
      </select>
       </li>
       <div id="add_sub_area" style="margin-left:15px;">
       	<?php foreach ($list_funcionario as $item_list_funcionario):
       			if($item_list_funcionario['funcionario_id']==$this->session->userdata('funcionario_id') && $item_list_funcionario['id_sub_area_laboral']!="")
				{ 
					echo "  <label>Deseas incluir tu subarea</label> (<b>".$item_list_funcionario['nombre_sub_area_laboral']."-".$item_list_funcionario['iniciales_sub_area_laboral']."</b>) <div style='display:inline'><input type='checkbox' $sub_area_remit name='id_sub_area' value='".$item_list_funcionario['id_sub_area_laboral']."'>
	<a class='tooltip'><img class='tooltips' src='". site_url('img/info_small.png'). "' />
         <span>Son las Unidades Internas de Cada Coordinacion. La correspondencia sandra con esa numeracion.</span></a>
         </div>";
				}
	    	endforeach;?>
       </div>
       <li>
       <label for="depen_destinatario">Dependencia Destinatario:</label>
        <?php echo form_error('list_area_laboral'); ?>
       <select name="list_area_laboral" id="list_area_laboral">
	  <?php foreach ($list_area_laboral as $item_area_laboral):?>
	  <option value="<?php echo $item_area_laboral['area_laboral_id']?>" 
	  <?php if($depend_destinatario==$item_area_laboral['nombre_area_laboral']){ echo "selected='selected'";} ?>>
	  <?php echo $item_area_laboral['nombre_area_laboral']?> </option>
	  <?php endforeach;?>
      </select>
       </li>
        <li>
       <label for="destinatario">Destinatario:</label>
        <?php echo form_error('list_funcionario'); ?>
       <select name="list_funcionario" id="list_funcionario">
       		<?php foreach ($list_func_destinatario as $item_list_func_destinatario):?>
	  			<option value="<?php echo $item_list_func_destinatario['funcionario_id']?>" 
	  			<?php if($name_destinatario==$item_list_func_destinatario['nombre_funcionario']){ echo "selected='selected'";} ?>>
	  			<?php echo $item_list_func_destinatario['nombre_funcionario']?> </option>
	  		<?php endforeach;?>
       
       
       </select>
		<?php
		if($confidencial=="disabled")
		{?>
		     <div id="text_copia" style="display:none;">
       			<a style='text-decoration: underline;color: red; cursor: pointer;' title='Con Copia' id='add_con_copia'>CC</a>
       		</div>
	<?php }
			else
			{?>
			<div id="text_copia" style="display:inline;">
       			<a style='text-decoration: underline;color: red; cursor: pointer;' title='Con Copia' id='add_con_copia'>CC</a>
       		</div>
			<?php }
		?>

       </li>
       <div id="content_con_copia">
       <?php if($func_id_copia<>""){?>
		
        	<li>
        		<label for="CC">CC</label>
                <div id="content_copia">
					<select name="list_funcionario" id="list_con_copia">
       					<?php foreach ($list_func_destinatario as $item_list_func_destinatario):?>
	  						<option value="<?php echo $item_list_func_destinatario['funcionario_id']?>" 
	  						<?php if($func_id_copia==$item_list_func_destinatario['funcionario_id']){ echo "selected='selected'";} ?>>
	  					<?php echo $item_list_func_destinatario['nombre_funcionario']?> </option>
	  					<?php endforeach;?>
					</select>
				</div>
            </li>
       <?php }?>
       </div>
       <li>
           <label for="asunto">Asunto:</label>
           <?php echo form_error('asunto'); ?>
           <input type="text" name="asunto"  id="asunto" placeholder="Asunto" maxlength="250" value="<?php
		echo ($a==1) ? $asunto : set_value('asunto'); ?> " autocomplete="off" />
       </li>
      
      
       <?php 
	   if( ! is_null($name_doc))
	  		{
				
		  	echo
			
			"<li><label for='file_adjunto'>Adjunto:</label>". anchor_popup(base_url().'int_enviada/reportes/descargar_archivo/'.
 			rawurlencode(str_replace("/","~",$ruta_doc)).'/'.rawurlencode($name_doc), $name_doc);
	 	  "</li>";}
		  else
		  {
			  echo "<li>
           <label for='adjuntar_archivo'>Adjuntar Archivo:</label>
            $error
           <input type='file' name='userfile' title='extensiones permitidas: doc,docx,xls,xlsx,ppt,pptx,pdf,jpg,gif,rar' id='userfile' style='padding-right:7px;'/>
           <a class='tooltip'><img class='tooltips' src='".site_url('img/info_small.png')."'  /><span>*formato:doc,docx,xls,xlsx,ppt,pdf,rar. *Sin Caracteres Especiales.</span></a>
       </li>";
}
		 ?>
       
       
       
       <li>
       	<label for="check_confidencial">Confidencial:</label>
        <input type="checkbox" <?php echo $confidencial?> name="confidencial" value="1" title="Solo podra verlo el usuario Destinatario, ningun otro del departamento de dicho usuario tendra acceso a esta correspondencia">
         <a class="tooltip"><img class="tooltips" src="<?php echo site_url('img/info_small.png') ?>" />
         <span>Solo podra verlo el Funcionario Destinatario, nadie mas perteneciente a ese departamento tendra acceso.</span></a>
       </li>
       <hr style="width: 900px; margin-left: -147px;">
       <br>
       <div style="margin-left: -100px;">
       <?php echo form_error('contenido'); ?>
                 <textarea  name="contenido" id="text_obser"><?php echo ($a==1) ? $contenido : @$_POST["contenido"];  ?></textarea>
        <span id="tx_char_rest" style="font-size:10px;">Copiar= Ctrl+c; Pegar=Ctrl+v</span>
        </div>
        <li style="text-align:center">
        
          
          <input type="button" name="vista_previa" id="btn_vista_previa" value="Vista Previa" style="margin: 0;
width: 100px;"><input type="submit" value="Enviar" style="margin:10px"> <div id="loading" style="display:none;">cargando...</div>

       </li>
    </ul>
    <input type="hidden" name="location" id="location" value="<?php echo $location; ?>"/>
<input type="hidden" name="id_corresp" value="<?php echo $id_corresp  ?>"/>
</form>
</div>

