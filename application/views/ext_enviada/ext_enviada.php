<div class="formulario">
<?php echo form_open_multipart('ext_enviada/ext_enviada') ?>
<ul>

	 <li>
       <?php
	   if($error_db==2)
	   {
			echo "<div class='error' style='text-align:center'>OCURRIO UN ERROR AL GUARDAR, INTENTE DE NUEVO</div>";
	   }
	   
	    /*?> <?php echo @$error_db;?><?php */?>
       <label for="depen_destinatario">Seleccione Remitente:</label>
        <?php echo form_error('list_funcionario_remitente'); ?>
       <select name="list_funcionario_remitente" id="list_funcionario_remitente">
	  <?php foreach ($list_funcionario as $item_list_funcionario):?>
	  <option value="<?php echo $item_list_funcionario['funcionario_id']?>" 
	  <?php if($item_list_funcionario['funcionario_id']==$this->session->userdata('funcionario_id')){ echo "selected='selected'";} ?>>
	  <?php echo $item_list_funcionario['nombre_funcionario']?> </option>
	  <?php endforeach;?>
      </select>
    </li>
	<li>
      <label for="label_destinatario">Dependencia Destinatario:</label>
      <?php echo form_error('depen_destinatario'); ?>
      <input type="text" name="depen_destinatario" id="entes_autocomplete" maxlength="100" placeholder="Ejemplo: GDC-Gobierno del Distrito Capital" value="<?php echo set_value('depen_destinatario'); ?>" />
    </li>
	<li>
      <label for="label_destinatario">Destinatario:</label>
       <?php echo form_error('destinatario'); ?>
      <input type="text" name="destinatario" id="personas_autocomplete" maxlength="100" value="<?php echo set_value('destinatario'); ?>" />
    </li>
    <li>
      <label for="asunto">Asunto:</label>
      <?php echo form_error('asunto'); ?>
      <input type="text" name="asunto" id="asunto"  placeholder="Asunto" maxlength="250" value="<?php echo set_value('asunto'); ?>" />
    </li>
    <li>
       <label for="label_dar_salida">Dar Salida en:</label>
       <?php echo form_error('ubic_recep'); ?>
       <select name="ubic_recep">
       
       <option value="seleccione">--SELECCIONE--</option>
	  <?php foreach ($list_recep as $item_recep):?>
	  <option value="<?php echo $item_recep['area_laboral_id']?>" 
	  <?php if($this->input->post('ubic_recep')===$item_recep['area_laboral_id']){ echo "selected='selected'";} ?>>
	  <?php echo $item_recep['nombre_area_laboral']?> </option>
	  <?php endforeach;?>
      </select>
 
    </li>
    <li>
       <textarea  name="contenido" id="text_obser" maxlength='2000'  placeholder="Contenido, Maximo 2000 caracteres..."></textarea>
       <div> <span id="tx_char_rest" style="font-size:10px;">Caracteres Restantes: 2000</span></div>
    </li>
    <li>
      <input type="submit" value="Enviar">
    </li>
</ul>
</div>

<?php
if($error_db==1)
{
	echo "<script>
	document.getElementById('entes_autocomplete').value='';
	document.getElementById('personas_autocomplete').value='';
	document.getElementById('asunto').value='';
	document.getElementById('text_obser').value='';
	alert('Se ha guardado con exito');
	</script>";
}

 ?>