<div class="formulario" style="width:90%;">
<?php echo form_open('int_recibida/reportes/remitente') ?>
<ul>
<li>
 <?php echo form_error('list_area_laboral'); ?>
<label for="depen_destinatario" style="text-align:right;">Remitente:</label>

  <select name="list_area_remitente" class="select_min" id="list_area_remitente">
  	<option value="">Seleccione</option>
	  <?php foreach ($list_area_laboral as $item_area_laboral):?>
	  <option value="<?php echo $item_area_laboral['area_laboral_id']?>" 
	  <?php if($this->input->post('list_area_remitente')===$item_area_laboral['area_laboral_id']){ echo "selected='selected'";} ?>>
	  <?php echo $item_area_laboral['nombre_area_laboral']?> </option>
	  <?php endforeach;?>
      <option value="1000" <?php if($this->input->post('list_area_remitente')==='1000') echo "selected='selected'" ?>>TODOS</option>
      </select>
             <div id="content_sub_area_laboral" style="display:inline">
       		<select name="list_sub_area_laboral" id="list_sub_area_laboral" class="select_min" title="sub area laboral">
       			<option value="0" class="option_sub_area_laboral">Seleccione-Sub Area Laboral</option>
       		</select>
       </div>
      <input type="text" readonly id="datepicker" name="datepicker" placeholder="Fecha Inicial" 
      value="<?php echo $this->input->post('datepicker'); ?>" title="fecha Inicial de envio"  style="cursor:pointer; width:100px;" required>
      <input type="text" readonly id="datepicker2" name="datepicker2" placeholder="Fecha Final" 
      value="<?php echo $this->input->post('datepicker2'); ?>" title="fecha Final de envio"  style="cursor:pointer; width:100px;" required>
 <input type="submit" value="Buscar" style="margin-left:0px;">
 
</li>

</ul>
<?php echo "<div class='error' style='text-align:center;'>".@$error_db."</div>"; ?>

</div>
<?php echo @$tabla; ?>

<?php echo  @$msg_10; ?>
