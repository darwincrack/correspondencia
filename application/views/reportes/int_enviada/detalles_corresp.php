<?php foreach ($item as $new_item):?>
<div id="table_correo" >
<div style="
    margin-left: 15px;
    margin-top: 10px;
"> <?php


 echo anchor_popup(base_url().'int_enviada/int_enviada/pdf/'.encrypt($new_item['corresp_id']), 'Imprimir', array('screenx' => '250'));?></div>
<table class="table_correo" >
  <tr>
      <td>NUM. CORRESPONDENCIA</td><td><?php echo $new_item['num_corresp_int']?></td>
  </tr>
  <tr>	
      <td>REMITENTE</td><td><?php echo $new_item['depend_remitente'].'-'.$new_item['name_remitente']?></td>
  </tr>
  <tr>	
      <td>DESTINATARIO</td><td><?php echo $new_item['depend_destinatario'].'-'. $new_item['name_destinatario']?></td>
  </tr>
  <tr>	
      <td>ASUNTO</td><td><?php echo $new_item['asunto']?></td>
  </tr>
  <tr> 
      <td>ADJUNTO</td>
      <td> 
      <?php if( ! is_null($new_item['name_doc']))
	  		{
		  	echo anchor_popup(base_url().'int_enviada/reportes/descargar_archivo/'.
 			rawurlencode(str_replace("/","~",$new_item['ruta_doc'])).'/'.rawurlencode($new_item['name_doc']), $new_item['name_doc']);
	 	 }?>
      </td>
  </tr>
  <tr>	
      <td>FECHA DE ENVIO</td><td><?php echo $new_item['fecha_enviado']?></td>
  </tr>
   <tr>	
      <td>ENVIADO POR</td><td><?php echo $new_item['enviado_por']?></td>
  </tr>
  <tr>	
      <td>FECHA RECIBIDO</td><td><?php echo $new_item['fecha_recibido']?></td>
  </tr>
  <tr>	
      <td>RECIBIDO POR</td><td><?php echo $new_item['recibido_por']?></td>
  </tr>
  <tr>	
      <td>CORREO AL DESTINATARIO</td><td><?php echo $new_item['correo']?></td>
  </tr>
   <tr>	
      <td width="35%">CORREO AL JEFE DEL DESTINATARIO</td><td><?php echo $new_item['correo_jefe_dest']?></td>
  </tr>
 
</table> 
</div>
<div class="content_correspondencia">
 <?php echo $new_item['contenido']?>
</div>
<?php endforeach;?>