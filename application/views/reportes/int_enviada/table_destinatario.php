<table class="table">
	<thead>
		<tr>
        	<th style="padding:0;"><?php echo "<a class='tooltip'><img src='".base_url()."img/Yes_check.svg.png' /><span>Correspondencia Leida</span></a>"; ?></th>
            
            <th style="padding:0;"><?php echo "<a class='tooltip'><img src='".base_url()."img/candado1.gif' /><span>Confidencial</span></a>"; ?></th>
            <th style="padding:0;"><?php echo "<a class='tooltip'><img width='14' height='14' src='".base_url()."img/top_alert_view-importance.gif'  /><span>Alerta de Correo no enviado</span></a>"; ?></th>
			<th>NUM CORRESP</th>
            <th>DEPEND. REMITENTE</th>
            <th>REMITENTE</th>
            <th>DEPEND. DESTINATARIO</th>
            <th>DESTINATARIO</th>
            <th>ASUNTO</th>
            <th>FECHA ENVIADO</th>
            <th>RESP</th>
		</tr>
	</thead>
	<tbody style="font-size:10px;">

			<?php foreach ($item as $new_item):
			$resp					=	'';
			$img_condencial			=	'';
			$img_leido				=	'';
			$img_alert_correo		=	'';
				if(($new_item['confidencial']=='1'))
				{
					$img_condencial="<img src='../../img/candado1.gif' width='14' height='14' />";
				}

				if($new_item['resp_corresp']!='')
				{
					$resp=  anchor(base_url()."int_recibida/reportes/int_recib_view/".encrypt($new_item['id_resp']).'/'.encrypt(0),$new_item['resp_corresp']);
				}
				
				if(($new_item['fecha_recibido']!=''))
				{
					$img_leido="<img src='../../img/Yes_check.svg.png' width='14' height='14' />";
				}
				
				if(($new_item['correo']=='ERROR' or $new_item['correo_jefe_dest']=='ERROR' ))
				{
					$img_alert_correo="<img src='../../img/alert_correo.png' width='14' height='14' />";
				}
				
				?>
		<tr>
        	<td><?php echo $img_leido ?></td>
        	<td><?php echo $img_condencial ?></td>
        	<td><?php echo $img_alert_correo ?></td>
			<td width="19%"><?php echo anchor(base_url()."int_enviada/reportes/int_env_view/".encrypt($new_item['id_corresp_int']),$new_item['num_corresp_int']);?></td>
            <td ><?php echo $new_item['depend_remitente']?></td>
			<td><?php echo $new_item['name_remitente']?></td>
			<td ><?php echo $new_item['depend_destinatario']?></td>
			<td><?php echo $new_item['name_destinatario']?></td>
			<td><?php echo $new_item['asunto']?></td>
			<td><?php echo $new_item['fecha_envio_correp']?></td>
            <td width="14%"><?php echo @$resp;?></td>
        </tr>

			<?php endforeach;?>
		</tr>
	</tbody>
</table>