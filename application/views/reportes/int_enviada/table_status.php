<table class="table">
	<thead>
		<tr>
            
            <th style="padding:0;"><?php echo "<a class='tooltip'><img src='".base_url()."img/candado1.gif' /><span>Confidencial</span></a>"; ?></th>
            <th style="padding:0;"><?php echo "<a class='tooltip'><img width='14' height='14' src='".base_url()."img/top_alert_view-importance.gif'  /><span>Alerta de Correo no enviado</span></a>"; ?></th>
			<th>#</th>
            <th>REMITENTE</th>
            <th>DESTINATARIO</th>
            <th>ASUNTO</th>
            <th>FECHA ENVIADO</th>
            <th>OBSERVACIONES</th>
            <th>STATUS</th>
		</tr>
	</thead>
	<tbody style="font-size:10px;">

			<?php foreach ($item as $new_item):
			$resp					=	'';
			$img_condencial			=	'';
			$img_alert_correo		=	'';
				if(($new_item['confidencial']=='1'))
				{
					$img_condencial="<img src='../../img/candado1.gif' width='14' height='14' />";
				}
				
				if(($new_item['correo']=='ERROR' or $new_item['correo_jefe_dest']=='ERROR' ))
				{
					$img_alert_correo="<img src='../../img/alert_correo.png' width='14' height='14' />";
				}
				
				?>
		<tr>
        	<td><?php echo $img_condencial ?></td>
        	<td><?php echo $img_alert_correo ?></td>
			<td><?php echo anchor(base_url()."int_enviada/int_enviada/modificar/".encrypt($new_item['id_corresp_int']).'/2/1',$new_item['id_hist']);?></td>
            <td ><?php echo '('.$new_item['depend_remitente'].'), '.$new_item['name_remitente']?></td>
            <td ><?php echo '('.$new_item['depend_destinatario'].'), '.$new_item['name_destinatario']?></td>

			<td><?php echo $new_item['asunto']?></td>
			<td><?php echo $new_item['fecha_envio_correp']?></td>
            <td><?php echo $new_item['observaciones']?></td>
            <td><?php echo $new_item['desc_status']?></td>
        </tr>

			<?php endforeach;?>
		</tr>
	</tbody>
</table>

