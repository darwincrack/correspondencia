<table class="table">
	<thead>
		<tr>
			<th>NUM CORRESP</th>
            <th>DEPEND. REMITENTE</th>
            <th>REMITENTE</th>
            <th>DEPEND. DESTINATARIO</th>
            <th>DESTINATARIO</th>
            <th>ASUNTO</th>
            <th>FECHA ENVIADO</th>
		</tr>
	</thead>
	<tbody>
			<?php foreach ($item as $new_item):?>
		<tr>
			<td width="19%"><?php echo anchor(base_url()."ext_enviada/ext_enviada_recep/view/".encrypt($new_item['id_corresp_ext']),$new_item['num_corresp_ext']);?></td>
			<td ><?php echo $new_item['depend_remitente']?></td>
			<td><?php echo $new_item['name_remitente']?></td>
			<td ><?php echo $new_item['depend_destinatario']?></td>
			<td><?php echo $new_item['name_destinatario']?></td> 
			<td><?php echo $new_item['asunto']?></td>
			<td width="18%"><?php echo $new_item['fecha_envio_correp']?></td>
        </tr>
			<?php endforeach;?>
		</tr>
	</tbody>
</table>