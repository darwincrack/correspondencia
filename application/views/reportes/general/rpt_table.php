<table class="table">
	<thead>
		<tr>
              <th style="padding:0;"><?php echo "<a class='tooltip'><img src='".base_url()."img/top_recibido_view-document.gif' /><span>Correspondencia Leida</span></a>"; ?></th>
            <th style="padding:0;"><?php echo "<a class='tooltip'><img src='".base_url()."img/candado1.gif' /><span>Confidencial</span></a>"; ?></th>
            <th style="padding:0;"><?php echo "<a class='tooltip'><img width='14' height='14' src='".base_url()."img/top_alert_view-importance.gif'  /><span>Alerta de Correo no enviado</span></a>"; ?></th>
			<th>NUM CORRESP</th>
            <th>DEPEND. REMITENTE</th>
            <th>REMITENTE</th>
            <th>DEPEND. DESTINATARIO</th>
            <th>DESTINATARIO</th>            
            <th>ASUNTO</th>
            <th>ESTADO</th>
            <th>FECHA ENVIADO</th>
            <th>RESP</th>
		</tr>
	</thead>
	<tbody>
			<?php foreach ($item as $new_item):
			$img_sobre			=	'';
			$img_condencial		=	'';
			$img_alert_correo	=	'';
			$resp	=	'';
			// 'cfcd208495d565ef66e7dff9f98764da'=0 EN MD5 no posee fecha de recibido
				if ($new_item['fecha_recib_corresp']=='cfcd208495d565ef66e7dff9f98764da')
				{
					$img_sobre="<img src='../../img/sobre.gif' width='14' height='14' />";
				}
				else
				{
					$img_sobre="<img src='../../img/msg-read.gif' />";
				}
				
				
				if(($new_item['confidencial']=='1'))
				{

					$img_condencial="<img src='../../img/candado1.gif' />";
				}
				
				
				if(($new_item['correo']=='ERROR' or $new_item['correo_jefe_dest']=='ERROR' ))
				{
					$img_alert_correo="<img src='../../img/alert_correo.png' width='14' height='14' />";
				}
				
				
				

				if($new_item['resp_corresp']!='')
				{
					$resp=  anchor(base_url()."reporte_general/reportes/detalles_view/".encrypt($new_item['id_resp']).'/',$new_item['resp_corresp']);
				}
			?>
		<tr>
        	<td><?php echo $img_sobre ?></td>
            <td><?php echo $img_condencial ?></td>
            <td><?php echo $img_alert_correo ?></td>
			<td width="20%"><?php echo $img2.$img. anchor(base_url()."reporte_general/reportes/detalles_view/".encrypt($new_item['id_corresp_int']),(is_null($new_item['num_corresp_int'])?"S/N":$new_item['num_corresp_int']));?> </td>
			<td ><?php echo $new_item['depend_remitente']?></td>
			<td><?php echo $new_item['name_remitente']?></td>
            <td><?php echo $new_item['depend_destinatario']?></td>
			<td><?php echo $new_item['name_destinatario']?></td>
            <td><?php echo $new_item['asunto']?></td>            
			<td><?php echo $new_item['status_desc']?></td>
			<td width="18%"><?php echo $new_item['fecha_envio_correp']?></td>
			<td><?php echo $resp?></td>


        </tr>
			<?php endforeach;?>
		</tr>
	</tbody>
</table>
