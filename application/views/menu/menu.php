<?php 

$show_menu="<ul id='menu'>";
 foreach ($rol_user as $rol_user_item):

switch ($rol_user_item['nombre']) {
    case 'ROL_CORRESP_INT_ENVIADA':
        $show_menu.= "<!--interna enviada-->
			<li><a>".$rol_user_item['descrip']."</a>
			<ul>
				<li><li>".anchor(base_url().'int_enviada/int_enviada/redactar', 'REDACTAR')."</li>
            	<li><a>REPORTES</a>
					<ul>
						<li>".anchor(base_url().'int_enviada/reportes/destinatario', 'POR DEPARTAMENTO DEST')."</li>
						<li>".anchor(base_url().'int_enviada/reportes/user_destinatario', 'POR FUNCIONARIO DEST')."</li>
					</ul>
				</li>
					<li>".anchor(base_url().'int_enviada/reportes/status', 'STATUS')."</li>
        	</ul>	
		</li>
    	<!--fin interna enviada-->";
    break;
    case 'ROL_CORRESP_INT_RECIBIDA':
        $show_menu.= "<!--Interna Recibida-->
    	<li>
    		<a>".$rol_user_item['descrip']."</a>
					<ul>
						<li>".anchor(base_url().'int_recibida/reportes/remitente', 'POR DEPARTAMENTO REMIT')."</li>
						<li>".anchor(base_url().'int_recibida/reportes/user_remitente', 'POR FUNCIONARIO REMIT')."</li>
					</ul>
    	</li>
    	<!--fin Interna Recibida-->";
    break;
    case 'ROL_CORRESP_EXT_ENVIADA':
        $show_menu.= "<!--Externa Enviada-->
    	<li><a>".$rol_user_item['descrip']."</a>
		<ul>
			<li><li>".anchor(base_url().'ext_enviada/ext_enviada', 'REDACTAR')."</li>
            <li><a>REPORTES</a>
				<ul>
					<li>".anchor(base_url().'ext_enviada/reportes/fecha_envio', 'FECHA DE ENVIO')."</li>
					<li>".anchor(base_url().'ext_enviada/reportes/destinatario', 'POR ENTE DESTINATARIO')."</li>
				</ul>
			</li>
        </ul>	
	</li>
    <!--fin externa enviada-->";
        break;
		
	case 'ROL_CORRESP_EXT_ENVIADA_RECEP':
        $show_menu.= "<!--Externa Enviada Recep-->
    	<li>".anchor(base_url().'ext_enviada/ext_enviada_recep',$rol_user_item['descrip'])."</li>
    <!--fin externa enviada recep-->";
        break;	
			
	case 'ROL_CORRESP_EXT_RECIBIDA':
		$show_menu.= "<!--Externa Recibida-->
    	<li><a>".$rol_user_item['descrip']."</a>
		<ul>
			<li>".anchor(base_url().'ext_recibida/ext_recibida', 'REDACTAR')."</li>
            <li><a href='http://www.ndesign-studio.com'>Reportes</a></li>
            </ul>	
	</li>
   <!--fin Externa Recibida-->";
	break;
	case 'ROL_REPORTES_GENERAL':
	$show_menu.= "<!--inicio Reporte General-->
    <li>
    <a>".$rol_user_item['descrip']."</a>
		<ul>
		   	<li>".anchor(base_url().'reporte_general/reportes', 'INTERNA')."</li>
			<li>".anchor(base_url().'reporte_general/reportes/externa', 'EXTERNA')."</li>
        </ul>
    </li>
    <!--fin Reporte General-->";
	break;	
}
 endforeach; 
 $show_menu.="  <!--inicio Salir-->
    <li>".anchor(base_url().'login/logout', 'SALIR')."
    </li>
    <!--fin Salir-->
	<div id='name_user'><span>USUARIO:".$this->session->userdata('username')."</span></DIV></ul>";
	$this->input->set_cookie('men_corresp',$this->encrypt->encode($show_menu),86500);
	echo $show_menu;
	
 /* End of file menu.php */
 /* Location: ./application/views/menu/menu.php */
 
