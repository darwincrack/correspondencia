<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta name="description" content="Sistema de Correspondencia">
<title><?php echo  $title ?></title>
<link rel="icon"  href="<?php echo  base_url()?>img/favicon.gif" />
<!--cargamos los css-->
<link rel="stylesheet" href="<?php echo  base_url()?>css/styles.css">
<?php echo  $_styles ?>
</head>
<body>
	<div id="agrupar">
		<!--el header se carga desde aqui-->
		<header id="cabecera">
        <img src="<?php echo  base_url()?>img/sat_top_main.jpg" />
		</header>

		<!--el menu dinamico-->
		<nav id="nav">
			<?php
			if($this->input->cookie('men_corresp',TRUE)==FALSE)
			{
				echo $header;
			}
			else
			{
				echo  $this->encrypt->decode($this->input->cookie('men_corresp'));
			}
			 ?>
		</nav>

		<!--contenido-->
		<section id="seccion">
			<header>
				<h1><?php echo $title ?></h1>
			</header>
			<div id="contenido">
				<?php echo $content ?>
			</div>	
		</section>
        
        
		<!--el footer se carga desde aqui-->
		<footer id="pie">
        	<span>Sistema de Correspondencia ©Copyright 2014<br/>
  			Reservados todos los Derechos - Servicio de Administración Tributaria del Distrito Capital (SATDC)<br/>
  			Por Unidad de Informática</span>
        </footer>
	</div>
<!--cargamos los js-->
<script src="<?php echo  base_url()?>js/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo  base_url()?>js/funciones/funciones.js"></script>
<?php echo $_scripts ?>
</body>

</html>