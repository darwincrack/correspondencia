<div class="formulario">
<?php echo form_open_multipart('ext_recibida/ext_recibida') ?>
	<ul>
    	<li>
        	<?php
			if($error_db==2)
	   		{
				echo "<div class='error' style='text-align:center'>OCURRIO UN ERROR AL GUARDAR, INTENTE DE NUEVO</div>";
	   		}?>
       		<label for="num_corresp">Numero:</label>
      		<?php echo form_error('num_corresp'); ?>
      		<input type="text" name="num_corresp" id="num_corresp"  placeholder="Numero de Correspondencia" maxlength="20" value="<?php echo set_value('num_corresp'); ?>" />
        </li>
		<li>
      		<label for="label_remitente">Dependencia Remitente:</label>
      		<?php echo form_error('depen_remitente'); ?>
      		<input type="text" name="depen_remitente" id="entes_autocomplete" maxlength="100" placeholder="Ejemplo: GDC-Gobierno del Distrito Capital" value="<?php echo set_value('depen_remitente'); ?>" />
    </li>
	<li>
      <label for="label_remitente">Remitente:</label>
       <?php echo form_error('remitente'); ?>
      <input type="text" name="remitente" id="personas_autocomplete" maxlength="100" value="<?php echo set_value('remitente'); ?>" />
    </li>
    <li>
         <label for="depen_destinatario">Dependencia Destinatario:</label>
          <?php echo form_error('list_area_laboral'); ?>
         <select name="list_area_laboral" id="list_area_laboral">
         	<option value="seleccione">--SELECCIONE--</option>
        	<?php foreach ($list_area_laboral as $item_area_laboral):?>
        	<option value="<?php echo $item_area_laboral['area_laboral_id']?>" 
        	<?php //if($this->input->post('list_area_laboral')===$item_area_laboral['area_laboral_id']){ echo "selected='selected'";} ?>>
        	<?php echo $item_area_laboral['nombre_area_laboral']?> </option>
        	<?php endforeach;?>
          </select>
     </li>
     <li>
       <label for="destinatario">Destinatario:</label>
        <?php echo form_error('list_funcionario'); ?>
       <select name="list_funcionario" id="list_funcionario">
       		<option value="seleccione">--SELECCIONE--</option>
       </select>
     </li>
     <li>
      <label for="asunto">Asunto:</label>
      <?php echo form_error('asunto'); ?>
      <input type="text" name="asunto" id="asunto"  placeholder="Asunto" maxlength="250" value="<?php echo set_value('asunto'); ?>" />
    </li>
    <li>
       <label for="fecha_correspondencia">Fecha Correspondencia:</label>
       <?php echo form_error('datepicker'); ?>
	   <input type="text" readonly id="datepicker" name="datepicker" value="<?php echo $this->input->post('datepicker'); ?>" title="fecha Correspondencia"  style="cursor:pointer;">
    </li>
        <li>
      <input type="submit" value="Enviar">
    </li>
	</ul>
    </form>
</div>
<?php
if($error_db==1)
{
	echo "<script>
	document.getElementById('num_corresp').value='';
	document.getElementById('entes_autocomplete').value='';
	document.getElementById('personas_autocomplete').value='';
	document.getElementById('asunto').value='';
	document.getElementById('datepicker').value='';
	alert('Se ha guardado con exito');
	</script>";
}

 ?>