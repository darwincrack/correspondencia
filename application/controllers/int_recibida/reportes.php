<?php class Reportes extends CI_Controller
{
var $estatus=TRUE;
		function __construct()
    {
       parent::__construct();
		$this->permiso_user_rol->validar_rol('ROL_CORRESP_INT_RECIBIDA');
		$this->load->model('listas/listas'); 
		$this->load->model('reportes/int_recibida'); 
		$this->load->library('encrypt');
		$this->load->library("mpdf");
		$this->load->helper('download');
		$this->load->helper('funciones');
    }

	function remitente()
	{
		$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery.ui.datepicker-es.js');
		$this->template->add_css('css/jquery-ui.css');
		
		
			if($this->estatus===TRUE)
			{
				
				//se obtiene las diez ultimas correspondencias
				$data['item']= $this->int_recibida->get_rpt_corresp_int_recib_teen();
				if(empty($data['item']))
				{ 
					$data['error_db']='NUNCA NADIE TE HA ENVIADO CORRESPONDENCIA';
				}
				else
				{
					$data['tabla'] = $this->load->view('reportes/int_recibida/table_remitente', $data, TRUE);
					$data['msg_10'] = "<div class='top_diez'><span>Ultimas 10 Correspondencias recibidas</span></div>";	
					
				}	
			}
			
			
			if( $this->input->post('list_area_remitente') && $this->estatus===TRUE && $this->input->post('datepicker')&& $this->input->post('datepicker2') )
			{
				
				$data['item']= $this->int_recibida->get_rpt_corresp_recib_remitente();
				if(empty($data['item']))
				{ 
				  $data['error_db']='NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS';
				  $data['tabla']='';
				  
				}
				else
				{
				  $data['tabla'] = $this->load->view('reportes/int_recibida/table_remitente', $data, TRUE);
				  
				}
			$data['msg_10']='';
			$this->estatus=FALSE;
			}
			
		  	 $data['list_area_laboral']= $this->listas->get_list_area_laboral();
		  	 $this->template->write_view('content', 'reportes/int_recibida/remitente',$data,TRUE);
		 	 $this->template->write('title', 'Filtrado por Dependencia del Remitente. <small>Interna Recibida</small>', TRUE);
		 	 $this->template->render();	
			
	}
	
	
	
	
	function user_remitente()
	{
		$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery.ui.datepicker-es.js');
		$this->template->add_css('css/jquery-ui.css');
		
				
		if( $this->input->post('lista_func_remitente') && $this->input->post('datepicker')&& $this->input->post('datepicker2') && $this->estatus===TRUE)
		{
			$data['item']= $this->int_recibida->get_rpt_corresp_env_remitente_user();
			if(empty($data['item']))
			{ 
				
				$data['error_db']=$this->session->userdata('id_area_laboral').'NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS';
			}
			else
			{
				$data['tabla'] = $this->load->view('reportes/int_recibida/table_remitente', $data, TRUE);
			}
			$this->estatus=FALSE;
			
			
		}
		$data['lista_func_remitente']= $this->listas->get_lista_func_remitente();
		$this->template->write_view('content', 'reportes/int_recibida/remitente_user',$data,TRUE);
		$this->template->write('title', 'Filtrado por Nombre del Remitente. <small>Interna Recibida</small>', TRUE);
		$this->template->render();	
	
	}
	
	function int_recib_view($id_corresp_int_env=FALSE,$fecha_recib_corresp=FALSE)
	{
		if($id_corresp_int_env != '' or $fecha_recib_corresp!='')
		{	
			$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
			$this->template->add_js('js/funciones/readmore.js');
			$this->template->add_js('js/tinymce/js/tinymce/tinymce.dev.js');
			$this->template->add_css('css/jquery-ui.css');
			$id_corresp_int_env=decrypt($id_corresp_int_env);
			$fecha_recib_corresp=decrypt($fecha_recib_corresp);
			//0 en md5=cfcd208495d565ef66e7dff9f98764da, no posee fecha de recibido
			//validar si se agrega la fecha de recibido
			if($fecha_recib_corresp=='0')
			{
				$this->_set_fecha_recibido($id_corresp_int_env);
				$this->_email_leido($id_corresp_int_env);
			}
			
			
			$data['item']= $this->int_recibida->get_rpt_corresp_recib_id($id_corresp_int_env);
			echo $this->encrypt->decode($id_corresp_int_env);
		
			if(empty($data['item']))
			{ 
				$this->template->write('content', "<div class='error' style='text-align:center;'>NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS</div> ", TRUE);
			}
			else
			{
				$this->template->write('title', 'DETALLES <small>Interna Recibida</small>', TRUE);
			 	$this->template->write_view('content', 'reportes/int_recibida/detalles_corresp',$data,TRUE);
			}
			$this->template->render();
		}
		else
		{
			show_404();

		}
		
	
	
	
	}
	
	
	function _set_fecha_recibido($id_corresp_int)
	{
		$this->load->model('int_recibida/int_recibida_model');
		$this->int_recibida_model->set_fecha_recib_corresp_int($id_corresp_int);
	}
	
	
	 public function descargar_archivo($ruta,$doc)
	{	
		$ruta=rawurldecode(str_replace("~","/",$ruta));
		$doc=rawurldecode($doc);
		$path = $ruta.$doc;
		header('Content-Type: text/html; charset=utf-8');
 		header("Content-Description: Descargar");
 		header("Content-Disposition: attachment; filename=$doc");
 		header("Content-Type: application/force-download");
 		header("Content-Length: " . filesize($path));
 		header("Content-Transfer-Encoding: binary");
 		readfile($path);
	}
	
	
	
	//pdf
		//el pdf final con la correspondencia ya guardada
	function pdf($corresp_id)
	{
		$corresp_id=decrypt($corresp_id);
		$item=$this->int_recibida->get_rpt_corresp_recib_id($corresp_id);
		
		if(! empty($item))
		{
		
		foreach ($item as $item_new):
		//CONTENIDO DEL PDF
		$data['num_corresp']	=	$item_new['num_corresp_int'];
		$data['destinatario']	=	$item_new['name_destinatario'];
		$data['dest_cargo']		=	$item_new['cargo_destinatario'];
		$data['remitente']		=	$item_new['name_remitente'];
		$data['remit_cargo']	=	$item_new['cargo_remitente'];
		$data['asunto']			=	$item_new['asunto'];
		$data['fecha_envio']	=	$item_new['fecha_enviado_pdf'];
		$data['contenido']		=	$item_new['contenido'];
		$data['name_con_copia']	=	$item_new['name_copia'];
		$data['cargo_con_copia']=	$item_new['cargo_copia'];
		// FIN CONTENIDO DEL PDF
		endforeach;
		
		
		$this->mpdf->mPDF('','letter','','',20,20,40,30,'L');
		$this->mpdf->SetDisplayMode(60);
		//$this->mpdf->mPDF('','letter','','',20,20,MT,MB,'L');
		$css_ruta=base_url().'css/pdf.css';
		$css = file_get_contents($css_ruta);
		$this->mpdf->WriteHTML($css,1);
		$html = $this->load->view('pdf/int_enviada_pdf', $data, true);
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output();
		}
		else
		{
			show_404();
		}
	}
	
	
	public function get_sub_area_laboral()
	{
        if($this->input->post('id_area_laboral'))
        {
            $id_area_laboral = $this->input->post('id_area_laboral');
            $sub_area_laboral= $this->listas->get_lista_sub_area_laboral($id_area_laboral);
            echo "<select name='list_sub_area_laboral' id='list_sub_area_laboral' class='select_min' title='sub area laboral'>";
			foreach($sub_area_laboral as $item_sub_area_laboral):
            
               echo " <option value='".$item_sub_area_laboral['id_sub_area_laboral']."' class='option_sub_area_laboral'>". $item_sub_area_laboral['desc_sub_area_laboral']."</option>";
           endforeach;
		   echo "</select>";
        }
	}
	
	
	
	
	
	private function _email_leido($id_corresp_int_env)
	{

		$item= $this->int_recibida->get_rpt_corresp_recib_id($id_corresp_int_env);
		
		
		if(! empty($item))
		{
		
			$this->load->library('email');
			$this->email->from('satgdc@gdc.gob.ve', 'Sistema de Correspondencia SATDC');
		
			foreach ($item as $item_new):
				  $correo_remitente			=	$item_new['correo_remitente'];
				  $data["num_corresp"]		=	$item_new['num_corresp_int'];
				  $data["fecha_enviado"]		=	$item_new['fecha_enviado'];
				  $data["fecha_recibido"]	=	$item_new['fecha_recibido'];	
				  $asunto					=	$item_new['asunto'];
			endforeach;
		
			$this->email->to($correo_remitente);
			$this->email->subject('Notificación de correspondencia Leida'.' ('.$asunto.')');
			$this->email->message($this->load->view('email/email_leido_view',$data,true));
		  
		//si se envia el correo, entonces se hace un update a la correspondecia para indicar 
		//que el correo fue enviado satisfactoriamente
		
				if ( ! $this->email->send())
				{	
					$this->email->print_debugger();
				}
			
		}
		else
		{
			show_404();
		}
	}
	
	
}

/* End of file reportes.php */
/* Location: .application/controllers/int_recibida/reportes.php */