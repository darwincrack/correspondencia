<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Int_enviada extends CI_Controller
{
	var $status_upload		=	TRUE; 	// estatus de la subida de archivos al servidor
	var $msg_error_upload 	= 	'';		// mensaje de error a la hora de subir archivos al servidor
	var $msg_error_db 		= 	'';		// mensajes de errores en los insert o update en la base de datos
	var $status_error_db 	= 	FALSE;	// estatus a la hora de interactuar con la base de datos
	var $name_file			=	''; 	// nombre del archivo adjunto
	var $ruta_file			=	''; 	// ruta de la carpeta del destintario en el servidor
	//var $ruta_pdf_preview	=	'/mnt/correspondencia/temp/'; 	// ruta donde se almacenaran los PDF temporales(preview)
	var $ruta_pdf_preview	=	'//192.168.0.133/apps/correspondencia/temp/'; 	// ruta donde se almacenaran los PDF temporales(preview)

	var $valido				=	'';
	var $respuesta 			=	''; 	//si es distinto a '' entonces es porque es una respuesta de correspondencia
	var $id_corresp			=	'';     //util para  respuestas de correspondencia
	var $list_funcionario	=	'';		//util para  respuestas de correspondencia
	var $list_area_laboral	=	'';		//util para  respuestas de correspondencia
	var $num_corresp		=	''; 	//captura el numero de corresp solo cuando es una respuesta
	var $n_remit_cargo		=	'';

	function __construct()
    {
       parent::__construct();
		$this->permiso_user_rol->validar_rol('ROL_CORRESP_INT_ENVIADA');
		$this->load->model('listas/listas'); 
		$this->load->model('int_enviada/int_enviada_model');
		$this->load->library('upload','email');
		$this->load->helper('file');
		$this->load->helper('funciones');
		$this->load->library("mpdf");
    }
	
	
	/**
	* Inicializa la ventana de enviar correspondencia
	* Valida si se adjunto un archivo 
	* @access	public
	* @ return void
	*/
	function redactar($location=FALSE,$respuesta=FALSE,$corresp_id=FALSE,$id_remit=FALSE,$id_depend_remit=FALSE,$num_corresp=FALSE,$n_remit_cargo=FALSE)
	{
		if($location!=FALSE && $location!=1) show_404();
		
		
		$this->template->add_js('js/tinymce/js/tinymce/tinymce.dev.js');
		$data['error']=$this->msg_error_upload;
		$data['error_db']=$this->msg_error_db;
		$data['valido']=$this->valido;
		$this->form_validation->set_rules('list_funcionario_remitente', 'Lista Remitente', 'callback_validar_select');
		$this->form_validation->set_rules('list_area_laboral', 'Lista Destinatarios', 'callback_validar_select');
		$this->form_validation->set_rules('list_funcionario', 'Lista Funcionarios', 'validar_select_funcionarios');
		$this->form_validation->set_rules('asunto', 'Asunto', 'required|trim||max_length[250]|xss_clean');
		$this->form_validation->set_rules('contenido', 'Contenido', 'required|trim');
		$this->form_validation->set_message('required', ' %s No puede estar en blanco');
		$this->form_validation->set_message('max_length', ' El campo %s no puede exceder de %s caracteres');
		$this->form_validation->set_error_delimiters('<div class="error">','</div>');
		
		
		if ($this->form_validation->run() === FALSE or $this->status_upload===FALSE or $this->status_error_db===TRUE)
		{	
			
			
			$data['list_funcionario']= $this->listas->get_lista_funcionarios($this->session->userdata('id_area_laboral'));
			$data['list_area_laboral']= $this->listas->get_list_area_laboral();
			
			
			//es porque es una respuesta de correspondencia
			if($location==1)
			{
				if( $this->input->post('id_corresp',TRUE) && $this->input->post('list_funcionario',TRUE) && $this->input->post('list_area_laboral',TRUE) && $this->input->post('num_corresp',TRUE) && $this->input->post('n_remit_cargo',TRUE))
				{
					$this->id_corresp			=	$this->input->post('id_corresp',TRUE);
					$this->list_funcionario		=	$this->input->post('list_funcionario',TRUE);
					$this->list_area_laboral	=	encrypt($this->input->post('list_area_laboral',TRUE));
					$this->num_corresp			=	$this->input->post('num_corresp');
					$this->n_remit_cargo		=	decrypt($this->input->post('n_remit_cargo',TRUE));
					
				}
				else
				{
					$this->num_corresp		=	$num_corresp;
					$this->id_corresp		=	$corresp_id;
					$this->list_funcionario	=	decrypt($id_remit);
					$this->list_area_laboral=	$id_depend_remit;
					$this->n_remit_cargo	=	urldecode($n_remit_cargo);
				}
					
				// se guardaran en los campos ocultos de la respuesta
				$data['resp_id_corresp']		=	$this->id_corresp;
				$data['resp_list_funcionario']	=	$this->list_funcionario;
				$data['resp_list_area_laboral']	=	$this->list_area_laboral;
				$data['resp_num_corresp']		=	$this->num_corresp;
				$num_corresp_title				=	decrypt($this->num_corresp);
				$data['resp_location']			=	$location;
				$data['respuesta']				=	$this->respuesta;
				$data['n_remit_cargo']			=	$this->n_remit_cargo; //
				
					
				$this->template->write("title", "Respuesta Correspondencia $num_corresp_title. <small>Interna Enviada</small>", TRUE);
					$this->template->write_view('content', 'int_enviada/respuesta_corresp',$data,TRUE);
			}
			
			else
			{
				$this->template->write('title', 'Redactar Correspondencia. <small>Interna Enviada</small>', TRUE);
				$this->template->write_view('content', 'int_enviada/int_enviada',$data,TRUE);	
			}
			
			$this->template->render();

			/*//es una respuesta de correspondencia
			if(($respuesta==1) or ($this->input->post('respuesta',TRUE)==1))
			{
				$this->respuesta		=	1;
				$this->num_corresp		=	decrypt($num_corresp);
				$this->id_corresp		=	$corresp_id;
				$this->list_funcionario	=	$id_remit;
				$this->list_area_laboral=	decrypt($id_depend_remit);
				
				if( $this->input->post('id_corresp',TRUE) && $this->input->post('list_funcionario',TRUE) && $this->input->post('list_area_laboral',TRUE) && $this->input->post('num_corresp',TRUE))
				{
					$this->id_corresp			=	$this->input->post('id_corresp',TRUE);
					$this->list_funcionario		=	$this->input->post('list_funcionario',TRUE);
					$this->list_area_laboral	=	$this->input->post('list_area_laboral',TRUE);
					
					$this->num_corresp			=	$this->input->post('num_corresp',TRUE);
					$num_corresp				=	$this->input->post('num_corresp',TRUE);
					
				}

				$this->template->write("title", "Respuesta Correspondencia [$this->num_corresp]. <small>Interna Enviada</small>", TRUE);
			}
			else
			{
				$this->template->write('title', 'Redactar Correspondencia. <small>Interna Enviada</small>', TRUE);
			}
			
			
				// se guardaran en los campos ocultos de la respuesta
				$data['resp_id_corresp']		=	$this->id_corresp;
				$data['resp_list_funcionario']	=	$this->list_funcionario;
				$data['resp_list_area_laboral']	=	$this->list_area_laboral;
				$data['resp_num_corresp']		=	$this->num_corresp;
				
				$data['resp_location']			=	$location;
				$data['respuesta']				=	$this->respuesta;

			if($location==1)
			{
				$this->template->write_view('content', 'int_enviada/respuesta_corresp',$data,TRUE);
			}
			else
			{
				$this->template->write_view('content', 'int_enviada/int_enviada',$data,TRUE);
			}
			
			$this->template->render();*/	
		}
		else
		{
			
			// se compreuba si hay algun archivo adjunto
			if (@$_FILES['userfile']['name']==TRUE)
			{	
				
				//$this->_validar_name_file($_FILES['userfile']['name']);
				/*if($this->status_upload===FALSE)
				{
					$this->index();
				}*/
				
				//se llama al metodo para subir archivo
        		$this->_upload_file($_FILES['userfile']['name']);
				if($this->status_upload===FALSE)
				{
					if($location!=1)
					{
						$this->redactar();
					}
					else
					{
						$this->redactar($location);
					}
				}
				
				//se inicializa el array que devuelve el metodo 'data()' y en la proxima linea se obtiene 
				// el valor de '[file_name]', el nombre del archivo subido
				$name_file=$this->upload->data();
				$this->name_file=$name_file['file_name'];
    	 	}
			
			//SI TODO SALE BIEN
			//si se subio un archivo con exito o si no se adjunto ningun archivo es status_upload es TRUE
			if($this->status_upload)
			{
				$ip						=	getRealIP();
				$corresp_id				=	decrypt($corresp_id);
				if($corresp_id=='') $corresp_id=NULL;
				
				
				if($this->input->post('list_con_copia',TRUE)) 
				{
					$list_con_copia=$this->input->post('list_con_copia',TRUE);
				}
				else
				{
					$list_con_copia=0;
				}
				
				$set_int_enviada		=	$this->int_enviada_model->set_int_enviada($this->name_file,$ip,$corresp_id,$list_con_copia);
				
				$estatus				=	$set_int_enviada->estatus; //el insert fue satisfactorio si es '1'
				$id_corresp_int			=	$set_int_enviada->id_corresp_int;
			/*	$num_corresp_int		=	$set_int_enviada->num_corresp_int;
				$descrip_depen_remit	=	$set_int_enviada->descrip_depen_remit;
				$name_remitente			=	$set_int_enviada->name_remitente;
				$asunto					=	$set_int_enviada->asunto;
				$correo_destinatario	=	$set_int_enviada->correo_destinatario;
				$correo_jefe_dest		=	$set_int_enviada->correo_jefe_destinatario;
				$name_destinatario		=	$set_int_enviada->name_destinatario;*/
				$status_desc			=	$set_int_enviada->status_desc;
				$status_id				=	$set_int_enviada->status_id;
				$id_historico			=	$set_int_enviada->id_historico;
				
				if($estatus==1)
				{
					

						// ////////////////////////////////
						if($status_id=="3")
						{

							$this->_enviar_correo_aprobado($id_corresp_int);
						}
						else
						{
							$this->_enviar_correo_no_aprobado($id_corresp_int,$id_historico,$status_desc);	
						}
						////////////////////////////////////
						//$this->msg_error_db='<div class="exito">SE HA GUARDADO CON EXITO y ENVIADO CORREO</div>';
					
				
						//$this->msg_error_db='<div class="exito">Enviado con Exito</div>';
					
					$this->valido=1;
					$this->status_error_db=TRUE;
					
					$id_corresp_int=encrypt($id_corresp_int);
					
					if($location!=1)
					{
						 
						echo "<script>
						window.open('../int_enviada/pdf/$id_corresp_int', '', 'width=800, height=500,left=300');
						window.location='redactar';
						</script>";
						
					}
					else
					{
						echo "<script>
						window.open('../../../../../../../../int_enviada/pdf/$id_corresp_int', '', 'width=800, height=500,left=300');
						window.location='../../../../../../../../../int_recibida/reportes/remitente';
						</script>";

					}
					
					
					
					
					
				}
				else
				{	//si ocurre un error al hacer el insert entonces se borra el archivo que se subio previamente
					$this->msg_error_db='<div class="error">HUBO UN PROBLEMA AL GUARDAR EN LA BD</div>';
					unlink($this->ruta_file.'/'.$this->name_file);
					$this->status_error_db=TRUE;
					$this->redactar();
				}
			}///
		}
				//	$this->template->render();

	}
	
	
	function modificar($id_corresp=FALSE,$location=FALSE,$a=FALSE)
	{
		
		if($a==1)
		{
			  $depend_destinatario="";
			  $data['a']=$a;
			  $data['location']=$location;
			  $data['id_corresp']=$id_corresp;
			  $this->load->model('reportes/interna_enviada'); 
			  $data['id_corresp']=$id_corresp;
			  $data['list_funcionario']= $this->listas->get_lista_funcionarios($this->session->userdata('id_area_laboral'));
			  $data['list_area_laboral']= $this->listas->get_list_area_laboral();
		  
			  $result= $this->interna_enviada->get_rpt_corresp_env_modif_id(decrypt($id_corresp));
				   
				  if(empty($result))
				  { 
					  $this->template->write('content', "<div class='error' style='text-align:center;'>NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS</div> ", TRUE);
				  }
				  else
				  {
					  foreach ($result as $results):
						  $data['asunto']= $results['asunto'];
						  $data['contenido']= $results['contenido'];
						  $data['name_destinatario']= $results['name_destinatario'];
						  $data['depend_destinatario']= $results['depend_destinatario'];
						  $depend_destinatario= $results['id_depend_destinatario'];
						  $data['name_remitente']= $results['name_remitente'];
						  $data['name_doc']= $results['name_doc'];
						  $data['ruta_doc']= $results['ruta_doc'];
						  $data['func_id_copia']= $results['func_id_copia'];
						  
						  
						  
						  if($results['sub_area_remit']<>0)
						  {
							  $data['sub_area_remit']="checked";
						  }
						  else
						  {
							  $data['sub_area_remit']="";
						  }
						  
						  
						  if($results['confidencial']==1)
						  {
							  $data['confidencial']="checked";
						  }
						  else
						  {
							  if($results['id_depend_destinatario']=="11")
							  {
									$data['confidencial']="disabled"; 
							  }
							  else
							  {
								 $data['confidencial']=""; 
							  }
							  
						  }
					  
					  endforeach;
					  
					  
					  $data['list_func_destinatario']= $this->listas->get_lista_funcionarios($depend_destinatario);
					  
					  
					  $this->template->add_js('js/tinymce/js/tinymce/tinymce.dev.js');
					  $this->template->write('title', 'Redactar Correspondencia. <small>Interna Enviada</small>', TRUE);
					  $this->template->write_view('content', 'int_enviada/redactar_view',$data,TRUE);	
				  }
			  $this->template->render();
		}// fin de a
		else
		{
			$this->template->add_js('js/tinymce/js/tinymce/tinymce.dev.js');
			$data['error']=$this->msg_error_upload;
			$data['error_db']=$this->msg_error_db;
			$data['valido']=$this->valido;
			$this->form_validation->set_rules('list_funcionario_remitente', 'Lista Remitente', 'callback_validar_select');
			$this->form_validation->set_rules('list_area_laboral', 'Lista Destinatarios', 'callback_validar_select');
			$this->form_validation->set_rules('list_funcionario', 'Lista Funcionarios', 'callback_validar_select');
			$this->form_validation->set_rules('asunto', 'Asunto', 'required|trim||max_length[250]|xss_clean');
			$this->form_validation->set_rules('contenido', 'Contenido', 'required|trim');
			$this->form_validation->set_message('required', ' %s No puede estar en blanco');
			$this->form_validation->set_message('max_length', ' El campo %s no puede exceder de %s caracteres');
			$this->form_validation->set_error_delimiters('<div class="error">','</div>');
			
			
			
			if ($this->form_validation->run() === FALSE)
			{	

				///
				$data['id_corresp']		=	$this->input->post("id_corresp");
				$data['location']		=	$this->input->post("location");
			
				$data['list_funcionario']= $this->listas->get_lista_funcionarios($this->session->userdata('id_area_laboral'));
				$data['list_area_laboral']= $this->listas->get_list_area_laboral();
				
				
	
				$this->template->write('title', 'Redactar Correspondencia. <small>Interna Enviada</small>', TRUE);
				$this->template->write_view('content', 'int_enviada/redactar_view',$data,TRUE);	
	
				
				$this->template->render();	
		}
		else
		{
			// se compreuba si hay algun archivo adjunto
			if (@$_FILES['userfile']['name']==TRUE)
			{	
				
				
				//se llama al metodo para subir archivo
        		$this->_upload_file($_FILES['userfile']['name']);
				if($this->status_upload===FALSE)
				{
						$this->modificar();

				}
				
				//se inicializa el array que devuelve el metodo 'data()' y en la proxima linea se obtiene 
				// el valor de '[file_name]', el nombre del archivo subido
				$name_file=$this->upload->data();
				$this->name_file=$name_file['file_name'];
    	 	}
			
			//SI TODO SALE BIEN
			//si se subio un archivo con exito o si no se adjunto ningun archivo es status_upload es TRUE
			if($this->status_upload)
			{
				$ip		=	getRealIP();
				if($this->input->post('list_con_copia',TRUE)) 
				{
					$list_con_copia=$this->input->post('list_con_copia',TRUE);
				}
				else
				{
					$list_con_copia=0;
				}
				
				$set_int_enviada		=	$this->int_enviada_model->update_int_enviada($this->name_file,$ip,$list_con_copia);
				
				$estatus				=	$set_int_enviada->estatus; //el insert fue satisfactorio si es '1'
				$id_corresp_int			=	$set_int_enviada->id_corresp_int;
				$num_corresp_int		=	$set_int_enviada->num_corresp_int;
				$descrip_depen_remit	=	$set_int_enviada->descrip_depen_remit;
				$name_remitente			=	$set_int_enviada->name_remitente;
				$asunto					=	$set_int_enviada->asunto;
				$correo_destinatario	=	$set_int_enviada->correo_destinatario;
				$correo_jefe_dest		=	$set_int_enviada->correo_jefe_destinatario;
				$name_destinatario		=	$set_int_enviada->name_destinatario;
				$status_desc			=	$set_int_enviada->status_desc;
				$status_id				=	$set_int_enviada->status_id;
				$id_historico			=	$set_int_enviada->id_historico;
				
				if($estatus==1)
				{
					
				
												// ////////////////////////////////
						if($status_id=="3")
						{
							
							$this->_enviar_correo_aprobado($id_corresp_int);
						}
						else
						{
							
							$this->_enviar_correo_no_aprobado($id_corresp_int,$id_historico,$status_desc);	
						}
						$this->msg_error_db='<div class="exito">SE HA GUARDADO CON EXITO y ENVIADO CORREO</div>';
					
					
						//$this->msg_error_db='<div class="exito">Enviado con Exito</div>';
				
					$this->valido=1;
					$this->status_error_db=TRUE;
					
					echo "<script>
						window.open('../../../pdf/$id_corresp', '', 'width=800, height=500,left=300');
						window.location='../../../redactar';
						</script>";
				}
				else
				{	//si ocurre un error al hacer el insert entonces se borra el archivo que se subio previamente
					$this->msg_error_db='<div class="error">HUBO UN PROBLEMA AL GUARDAR EN LA BD</div>';
					unlink($this->ruta_file.'/'.$this->name_file);
					$this->status_error_db=TRUE;
					$this->modificar();
				}
			}
		
		
		
		}
	}
	}
	
	
	/**
	* Obtiene de la BD la lista de funcionario, dependiendo el area laboral(llena el select)
	* @access	public
	* return 	void
	*/
	public function get_funcionarios()
	{
        if($this->input->post('id_area_laboral'))
        {
            $id_area_laboral = $this->input->post('id_area_laboral');
            $funcionario= $this->listas->get_lista_funcionarios($id_area_laboral);
            foreach($funcionario as $item_funcionario):
            ?>
                <option value="<?php echo $item_funcionario['funcionario_id'] ?>"><?php echo $item_funcionario['nombre_funcionario'] ?></option>
            <?php
           endforeach;
        }
	}
	
	
	
	/**
	* Sube archivos a la carpeta del destinatario 
	* contiene la configuracion de los parametros para subir los archivos
	* @access	private
	* return 	return
	*/
	
	function _upload_file($name_file)
	{  
		//valida si el nombre del archivo posee caracteres no validos
		if(preg_match('/[^a-zA-Z0-9\-_.]/',limpia_espacios($name_file)))
		{
			$this->status_upload=FALSE;
			$this->msg_error_upload='<p>No se permite este nombre de archivo</p>';
			return;
		}
		
		
		$item_ruta_file=$this->listas->get_ruta_file();
		
		if($item_ruta_file==TRUE)
		{
			$this->ruta_file=$item_ruta_file->ruta_file;
			$config['upload_path'] = $this->ruta_file;
			$config['allowed_types'] = 'gif|jpg|png|xlsx|pdf|txt|doc|docx|xls|pptx|ppt|';
			$config['max_size'] = '1024';
			$config['max_filename'] = '50';
			$config['remove_spaces'] = TRUE;
			$this->upload->initialize($config);
		}
		else
		{
			$this->status_upload=FALSE;
			$this->msg_error_upload='<p>No existe una ruta valida</p>';
			return;
		}
		//si la subida del archivo falla
		if ( ! $this->upload->do_upload())
		{
			$this->status_upload=FALSE;
			$this->msg_error_upload= $this->upload->display_errors();
			return;
		}
	}
	
	/**
	* Valida los select tanto de dependencia destinatario, como de destinatario
	*
	* @access	private
	* @param	string
	* return 	boolean
	*/
	
	function validar_select($str)
	{
		
		if ($str == 'seleccione' or $str==FALSE)
		{
			$this->form_validation->set_message('validar_select','Debe selecionar una opcion');
			return FALSE;
		}
		return TRUE;
	}
	
	function validar_select_funcionarios($str)
	{
		$xx= $this->input->post("list_area_laboral");
			
			
			if( $xx!="150")
			{
				if ($str == 'seleccione' or $str==FALSE)
				{
					$this->form_validation->set_message('validar_select_funcionarios','Debe selecionar una opcion');
					return FALSE;
				}
				
			}
			
			
		return TRUE;
	}
	
	/**
	* Envia correo al destinatario
	*
	* @access	privado
	* @param	int
	* @param	string
	* @param	string
	* @param	string
	* @param	string
	* @param	string
	* return 	void
	*/
	
	/*function _enviar_correo($id_corresp_int,$num_corresp,$depen_remit,$remit,$asunto,$dest_correo,$correo_jefe_dest,$dest_name,$status_desc,$id_historico)
	{
		echo "orige";
		$this->load->library('email');
		$this->email->from('satgdc@gdc.gob.ve', 'Sistema de Correspondencia SATDC');
		$this->email->to($dest_correo);
		
		if($dest_correo != $correo_jefe_dest)
		{
			$this->email->cc($correo_jefe_dest);
		}
		$this->email->subject('TIENES UNA NUEVA CORRESPONDENCIA DE '.$depen_remit);

			$this->email->message($this->_get_msg_email($num_corresp,$depen_remit,$remit,$asunto,$dest_name,$status_desc,$id_historico));
		
		
		//si se envia el correo, entonces se hace un update a la correspondecia para indicar 
		//que el correo fue enviado satisfactoriamente
		if ( $this->email->send()==TRUE)
		{	
			$this->int_enviada_model->update_corresp_int_correo($id_corresp_int,$dest_correo,$correo_jefe_dest);
		}
		else
		{
			//echo 'salir';
			//echo $this->email->print_debugger();
			$this->int_enviada_model->update_corresp_int_correo($id_corresp_int,'ERROR','ERROR');
		}
	}
	
	
	
	///// enviar correo al jefe remitente
	
	function _enviar_correo_jefe_remit($id_corresp_int,$num_corresp,$depen_remit,$remit,$asunto,$dest_correo,$dest_name,$status_desc,$id_historico)
	{
		echo "sfsdfsd";
		$this->load->library('email');
		$this->email->from('satgdc@gdc.gob.ve', 'Sistema de Correspondencia SATDC');
		$this->email->to($dest_correo);
		
		$this->email->subject('TIENES UNA NUEVA CORRESPONDENCIA POR APROBAR DE '.$depen_remit);

			$this->email->message($this->_get_msg_email($num_corresp,$depen_remit,$remit,$asunto,$dest_name,$status_desc,$id_historico));
		
		
		//si se envia el correo, entonces se hace un update a la correspondecia para indicar 
		//que el correo fue enviado satisfactoriamente
		if ( $this->email->send()==TRUE)
		{	
			$this->int_enviada_model->update_corresp_int_correo($id_corresp_int,$dest_correo,$correo_jefe_dest);
		}
		else
		{
			echo 'salirxx';
			echo $this->email->print_debugger();
		//	$this->int_enviada_model->update_corresp_int_correo($id_corresp_int,'ERROR','ERROR');
		}
	}
	
	*/
	
	
	
	
	
	
	
	
	
	
	/**
	* retorna el mensaje que sera enviado al correo del destintario en una tabla ya con formato
	*
	* @access	private
	* @param	string
	* @param	string
	* @param	string
	* @param	string
	* return 	string
	*/

/*	function _get_msg_email($num_corresp,$depen_remit,$remit,$asunto,$dest_name,$status_desc,$id_historico)
	{
		
		if($status_desc=="aprobado")
		{
			$table="<table>
					<tr>
						<th scope='row' width='270px' align='left'>NUM. CORRESPONDENCIA:</th><td>$num_corresp</td>
					</tr>
					<tr>
						<th scope='row' align='left'>DEPENDENCIA REMITENTE:</th><td>$depen_remit</td>
					</tr>
					<tr>
						<th scope='row' align='left'>REMITENTE:</th><td>$remit</td>
					</tr>
					<tr>
						<th scope='row' align='left'>DESTINATARIO:</th><td>$dest_name</td>
					</tr>
					<tr>
						<th scope='row' align='left'>ASUNTO:</th><td width='400px'>$asunto</td>
					</tr>
				</table>";
			
		}
		///
		else
		{
			$table="<table>
					<tr>
						<th scope='row' width='270px' align='left'>NUMERO REFERENCIAL:</th><td>$id_historico</td>
					</tr>
					<tr>
						<th scope='row' align='left'>DEPENDENCIA REMITENTE:</th><td>$depen_remit</td>
					</tr>
					<tr>
						<th scope='row' align='left'>REMITENTE:</th><td>$remit</td>
					</tr>
					<tr>
						<th scope='row' align='left'>DESTINATARIO:</th><td>$dest_name</td>
					</tr>
					<tr>
						<th scope='row' align='left'>ASUNTO:</th><td width='400px'>$asunto</td>
					</tr>
					<tr>
						<th scope='row' align='left'>ESTATUS:</th><td width='400px'>$status_desc</td>
					</tr>
				</table>";
		}
		return $table;
	}*/

	
	//crea el preview del pdf
	function create_temp_pdf()
	{
		
		$id_destinatario	=	$this->input->post('id_destinatario',TRUE);
		$id_remitente		=	$this->input->post('id_remitente',TRUE);
		$dest_con_copia		=	$this->input->post('dest_con_copia',TRUE);
		$asunto				=	$this->input->post('asunto',TRUE);
		$contenido			=	$this->input->post('contenido');
		
		
		$dest_remit_id=$this->int_enviada_model->get_pdf_preview($id_remitente,$id_destinatario,$dest_con_copia	);
		
		
		$i=1;
		foreach ($dest_remit_id as $item_dest_remit_id):
			if($i==1)
			{
				$name_remitente=$item_dest_remit_id['nombre'];
				$cargo_remitente=$item_dest_remit_id['cargo'];
			}
			else if($i==2)
			{
				$name_destinatario=$item_dest_remit_id['nombre'];
				$cargo_destinatario=$item_dest_remit_id['cargo'];				
			}
			else if($i==3)
			{
				$name_con_copia=$item_dest_remit_id['nombre'];;
				$cargo_con_copia=$item_dest_remit_id['cargo'];
			}
			$fecha =$item_dest_remit_id['fecha'];
			$i++;
		endforeach;
		
	 	//Especificamos algunos parametros del PDF
		$this->mpdf->mPDF('','letter','','',20,20,40,30,'L');
		//$this->mpdf->mPDF('','letter','','',20,20,MT,MB,'L');
		$css_ruta=base_url().'css/pdf.css';
		$css = file_get_contents($css_ruta);
		$this->mpdf->WriteHTML($css,1);
		
		
        //CONTENIDO DEL PDF
		$data['num_corresp']	=	'SATDC-XXXX-XXX-XX';
		$data['destinatario']	=	$name_destinatario;
		$data['dest_cargo']		=	$cargo_destinatario;
		$data['remitente']		=	$name_remitente;
		$data['remit_cargo']	=	$cargo_remitente;
		$data['name_con_copia']	=	$name_con_copia;
		$data['cargo_con_copia']=	$cargo_con_copia;
		$data['asunto']			=	$this->input->post('asunto');
		$data['fecha_envio']	=	$fecha;
		$data['contenido']		=	$this->input->post('contenido');
		// FIN CONTENIDO DEL PDF
		
		$this->mpdf->SetWatermarkText('VISTA PREVIA');
		$this->mpdf->showWatermarkText = true;
		$this->mpdf->SetDisplayMode(60);
		//cargamos la vista que contiene el template del pdf
		$html = $this->load->view('pdf/int_enviada_pdf', $data, true);
        //ESCRIBIMOS AL PDF
		
        $this->mpdf->WriteHTML($html);

		//nombre  aleatorio del pdf temporal
		$name_pdf=md5(uniqid(rand(),true));
		
		//ruta donde se guardara el archivo temporal

		$ruta_pdf_temp="$this->ruta_pdf_preview$name_pdf.pdf";
		//almacenamos el pdf PDF
		$this->mpdf->Output($ruta_pdf_temp,'F');
		echo "$name_pdf";
		
	  
	}
	
		// muestra el preview del pdf
	function preview($name_pdf)
	{
		// Vamos a mostrar un PDF
		header('Content-type: application/pdf');
		readfile("$this->ruta_pdf_preview$name_pdf.pdf");
	  }
	
	
	//el pdf final con la correspondencia ya guardada
	function pdf($corresp_id)
	{
		$this->load->model('reportes/interna_enviada');
		$corresp_id=decrypt($corresp_id);
		$item=$this->interna_enviada->get_rpt_corresp_env_id($corresp_id);
		
		if(! empty($item))
		{
		
		foreach ($item as $item_new):
			//CONTENIDO DEL PDF
			$data['num_corresp']	=	$item_new['num_corresp_int'];
			$data['destinatario']	=	$item_new['name_destinatario'];
			$data['dest_cargo']		=	$item_new['cargo_destinatario'];
			$data['remitente']		=	$item_new['name_remitente'];
			$data['remit_cargo']	=	$item_new['cargo_remitente'];
			$data['asunto']			=	$item_new['asunto'];
			$data['fecha_envio']	=	$item_new['fecha_enviado_pdf'];
			$data['contenido']		=	$item_new['contenido'];
			$data['name_con_copia']	=	$item_new['name_copia'];
			$data['cargo_con_copia']=	$item_new['cargo_copia'];
			$data['status']			=	$item_new['id_status'];
			$status_corresp			=	$item_new['id_status'];
			// FIN CONTENIDO DEL PDF
		endforeach;
		
		
		$this->mpdf->mPDF('','letter','','',20,20,40,30,'L');
		$this->mpdf->SetDisplayMode(60);
		//$this->mpdf->mPDF('','letter','','',20,20,MT,MB,'L');
		$css_ruta=base_url().'css/pdf.css';
		$css = file_get_contents($css_ruta);
		$this->mpdf->WriteHTML($css,1);
		
		if($status_corresp<>"3")
		{
			$this->mpdf->SetWatermarkText('POR APROBAR');
			$this->mpdf->showWatermarkText = true;
			$this->mpdf->SetDisplayMode(60);
		}
		
		$html = $this->load->view('pdf/int_enviada_pdf', $data, true);
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output();
		}
		else
		{
			show_404();
		}
	}


//respues de correspondencia
	function respuesta($corresp_id,$id_remit,$id_depend_remit,$num_corresp)
	{	  
		  $corresp_id		=	decrypt($corresp_id);	
		  $id_remit			=	decrypt($id_remit);	
		  $id_depend_remit	=	decrypt($id_depend_remit);	
		  $num_corresp		=	decrypt($num_corresp);	
		// $this->template->add_js('js/tinymce/js/tinymce/tinymce.dev.js');
		 //obtener remitente
		/*$corresp_id		=	decrypt($corresp_id);	
		  $id_remit			=	decrypt($id_remit);	
		  $id_depend_remit	=	decrypt($id_depend_remit);	
		  $num_corresp		=	decrypt($num_corresp);	
		  $data['list_funcionario']= $this->listas->get_lista_funcionarios($this->session->userdata('id_area_laboral'));
		  $this->template->write("title", "Respuesta Correspondencia [$num_corresp]. <small>Interna Enviada</small>", TRUE);
		  $this->template->write_view('content', 'int_enviada/respuesta_corresp',$data,TRUE);
		  $this->template->render();*/
		  $this->index(1,$corresp_id,$id_remit,$id_depend_remit,$num_corresp);
		  
		  
		
	}

	
	
		public function get_sub_area_laboral_func()
	{
        if($this->input->post('id_funcionario',TRUE))
        {
            $id_funcionario = $this->input->post('id_funcionario');
            $item= $this->int_enviada_model->get_sub_area_laboral_func($id_funcionario);
			if(empty($item))
			{ 
				echo "404";
			}
			else
			{
            	foreach($item as $item_sub_area):
echo "  <label>Deseas incluir tu subarea</label> (<b>".$item_sub_area['nombre_sub_area_laboral']."-".$item_sub_area['iniciales_sub_area_laboral']."</b>) <div style='display:inline'><input type='checkbox' name='id_sub_area' value='".$item_sub_area['id_sub_area_laboral']."'>
	<a class='tooltip'><img class='tooltips' src='". site_url('img/info_small.png'). "' />
         <span>Son las Unidades Internas de Cada Coordinacion.</span></a>
         </div>";
              
           		endforeach;
			}
        }
	}
	
	
	function modal_status()
	{
		
		
		if($this->session->userdata('coord_sub_area')<>0 or  $this->session->userdata('coord_area')<>0)
		{
			$id_corresp=$this->input->post("id_corresp");
			$data= $this->listas->get_lista_status();
			//$value= $this->listas->get_status_corresp($id_corresp);
			
			
			
			//if($item_data['id']==$value->id_status) echo "selected=selected"
			
			//echo "<label>Status Actual:</label><select>";
			
			echo "<div class='formulario' style='  margin: 0;
		  width: 50%;
		  margin-bottom: 15px;'>
		<label>Seleccione Estado:</label><select id='selec_status' style='width: 200px;
		  margin-bottom: 15px;'>";
				 foreach ($data as $item_data):
				 
				echo "<option value='".$item_data['id']."'";
				
				//if($item_data['id']==$value->id_status) echo "selected=selected";
				
				echo">
			   ".$item_data['descripcion']." </option>";
			  endforeach;
			  echo "</select>
			  <textarea maxlength='1000' placeholder='Observaciones, Max 1000 caracteres' style='  margin-bottom: inherit;
		  width: 300px;' id='text'></textarea>
		  <br>
			  <input type='button' value='Guardar' id='btn_guardar' onClick='update_corresp()'>
			  <div id='info'></div>
			  </div>";
			
		}
		
		else
		{
			show_error('NO TIENE PERMISO PARA VER ESTA PAGINA');
		}

	}
	
	function update_status()
	{
	
		$corresp_id=$this->input->post("id_corresp");
		$id_status=$this->input->post("id_status");	
		$text=$this->input->post("text");	
		$item=$this->int_enviada_model->status_corresp_env($corresp_id,$id_status,$text);
		if($id_status==3)
		{
			$this->_enviar_correo_aprobado($corresp_id);
		}
		else
		{
			$this->_enviar_correo_modificado($corresp_id);
		}

		echo "1";

	}
	
	
	/////////////////////////////
	
	
	function _enviar_correo_aprobado($id_corresp_int)
	{
		$this->load->model('reportes/interna_enviada');
		$this->load->library('email');
		$this->email->from('satgdc@gdc.gob.ve', 'Sistema de Correspondencia SATDC');
		
		$item=$this->interna_enviada->get_correo_corresp_env_id($id_corresp_int);
		$i=1;
		if(! empty($item))
		{
		
			foreach ($item as $item_new):
				  //CONTENIDO DEL PDF
				  $num_corresp			=	$item_new['num_corresp_int'];
				  $depen_remit			=	$item_new['descrip_depen_remit'];
				  $remit				=	$item_new['nombre_remitente'];
				  $dest_name			=	$item_new['nombre_destinatario'];
				  $nombre_con_copia		=	$item_new['nombre_con_copia'];
				  $asunto				=	$item_new['asunto'];
				  $list 				= 	array();
				  
				  if($i==1)
				  {
					  if($item_new['correo_destinatario'] !="")
					  {
						 $list[]	= $item_new['correo_destinatario'];
					  }
						
				  
					  if($item_new['correo_con_copia'] !="")
					  {
						 $list[]= $item_new['correo_con_copia'];
					  }
				  }
				  
				  if($item_new['correo_destinatario'] != $item_new['correo_jefe_area_destinatario'])
			  	  {
				  		if($item_new['correo_jefe_area_destinatario'] !="")
				  		{
					  		$list[]=$item_new['correo_jefe_area_destinatario'];
				  		}
			  	  }
				  
				  
			  	$this->email->to($list);
			  	$this->email->subject('Nueva Correspondencia de '.$item_new['descrip_depen_remit'].' ('.$asunto.')');

			  	$this->email->message($this->_get_msg_email_aprobado($num_corresp,$depen_remit,$remit,$asunto,$dest_name,$nombre_con_copia));
		  
			  
			  //si se envia el correo, entonces se hace un update a la correspondecia para indicar 
			  //que el correo fue enviado satisfactoriamente
			  if( ! empty($list))
			  {
				  if ( $this->email->send()==TRUE)
				  {	
					  $this->int_enviada_model->update_corresp_int_correo($id_corresp_int,$item_new['correo_destinatario'],$item_new['correo_jefe_area_destinatario']);
				  }
				  else
				  {
					  $this->email->print_debugger();
					  $this->int_enviada_model->update_corresp_int_correo($id_corresp_int,'ERROR','ERROR');
					  
				  }
			  }
			  
			$i++;
		endforeach;
		
	}
	else
	{
		show_404();
	}
}

	function _enviar_correo_no_aprobado($id_corresp_int,$id_historico,$status_desc)
	{
		$this->load->model('reportes/interna_enviada');
		$this->load->library('email');
		$this->email->from('satgdc@gdc.gob.ve', 'Sistema de Correspondencia SATDC');
		
		$item=$this->interna_enviada->get_correo_por_aprobar_env_id($id_corresp_int);
		
		if(! empty($item))
		{
		
			foreach ($item as $item_new):
				  //CONTENIDO DEL PDF
				  $num_corresp		=	$item_new['num_corresp_int'];
				  $depen_dest		=	$item_new['descrip_depen_dest'];
				  $remit			=	$item_new['nombre_remitente'];
				  $dest_name		=	$item_new['nombre_destinatario'];
				  $nombre_con_copia	=	$item_new['nombre_con_copia'];
				  $asunto			=	$item_new['asunto'];
				  $list 				= 	array();
				  
				  
			  		
				   if($item_new['correo_jefe_sub_area_remitente'] != $item_new['correo_remitente'])
				  {
				  	 	if($item_new['correo_jefe_sub_area_remitente'] !="")
				  		{
							$list[]=$item_new['correo_jefe_sub_area_remitente'];
				  		}
				  }

				  if($item_new['correo_jefe_remit'] !="")
				  {
						$list[]=$item_new['correo_jefe_remit'];
				  }
				 
				 
				  

			  	$this->email->to($list);
			  	$this->email->subject('Correspondencia por Aprobar'.' ('.$asunto.')');
			  
			  	$this->email->message($this->_get_msg_email_no_aprobado($num_corresp,$depen_dest,$remit,$asunto,$dest_name,$nombre_con_copia,$id_historico,$status_desc));
		  
			  
			  //si se envia el correo, entonces se hace un update a la correspondecia para indicar 
			  //que el correo fue enviado satisfactoriamente
			  if ( ! $this->email->send())
			  {	
				  $this->email->print_debugger();
			  }
			  
			
		endforeach;
		
	}
	else
	{
		show_404();
	}
	
	
	}
	
	
	function _enviar_correo_modificado($id_corresp_int)
	{
		$this->load->model('reportes/interna_enviada');
		$this->load->library('email');
		$this->email->from('satgdc@gdc.gob.ve', 'Sistema de Correspondencia SATDC');
		
		$item=$this->interna_enviada->get_correo_corresp_env_id($id_corresp_int);
		
		if(! empty($item))
		{
		
			foreach ($item as $item_new):
				  $num_corresp		=	$item_new['num_corresp_int'];
				  $depen_dest		=	$item_new['descrip_depen_dest'];
				  $remit			=	$item_new['nombre_remitente'];
				  $dest_name		=	$item_new['nombre_destinatario'];
				  $nombre_con_copia	=	$item_new['nombre_con_copia'];
				  $asunto			=	$item_new['asunto'];
				  $id_historico		=	$item_new['id_historico'];
				  $status_desc		=	$item_new['status_desc'];
				  $list 			= 	array();
				  
				  
			  		
				if($item_new['correo_remitente'] !="")
				 {
					$list[]=$item_new['correo_remitente'];
				 }	
		endforeach;
		
		$this->email->to($list);
		$this->email->subject('Correspondencia por Revisar'.' ('.$asunto.')');
		$this->email->message($this->_get_msg_email_no_aprobado($num_corresp,$depen_dest,$remit,$asunto,$dest_name,$nombre_con_copia,$id_historico,$status_desc));
		  
		//si se envia el correo, entonces se hace un update a la correspondecia para indicar 
		//que el correo fue enviado satisfactoriamente
		if( ! empty($list))
		{
			if ( ! $this->email->send())
			{	
				$this->email->print_debugger();
			}
		}	
	}
	else
	{
		show_404();
	}	
	
	}
	
	 function _get_msg_email_aprobado($num_corresp,$depen_remit,$remit,$asunto,$dest_name,$name_con_copia)
	{
		
		if($name_con_copia=="")
		{
			$table="<div style='text-align:justify'>Usted ha recibido una correspondencia desde el sistema automatizado de envio de correspondencia del SATDC.
La misma puede ser vista y respondida desde el siguiente link: http://correspondencia.satdc.gob.ve</div><br><br><table>
					<tr>
						<th scope='row' width='270px' align='left'>NUM. CORRESPONDENCIA:</th><td>$num_corresp</td>
					</tr>
					<tr>
						<th scope='row' align='left'>DEPENDENCIA REMITENTE:</th><td>$depen_remit</td>
					</tr>
					<tr>
						<th scope='row' align='left'>REMITENTE:</th><td>$remit</td>
					</tr>
					<tr>
						<th scope='row' align='left'>DESTINATARIO:</th><td>$dest_name</td>
					</tr>
					<tr>
						<th scope='row' align='left'>ASUNTO:</th><td width='400px'>$asunto</td>
					</tr>
				</table>";
	}
	else
	{
			$table="<div style='text-align:justify'>Usted ha recibido una correspondencia desde el sistema automatizado de envio de correspondencia del SATDC.
La misma puede ser vista y respondida desde el siguiente link: http://correspondencia.satdc.gob.ve</div><br><br><table>
					<tr>
						<th scope='row' width='270px' align='left'>NUM. CORRESPONDENCIA:</th><td>$num_corresp</td>
					</tr>
					<tr>
						<th scope='row' align='left'>DEPENDENCIA REMITENTE:</th><td>$depen_remit</td>
					</tr>
					<tr>
						<th scope='row' align='left'>REMITENTE:</th><td>$remit</td>
					</tr>
					<tr>
						<th scope='row' align='left'>DESTINATARIO:</th><td>$dest_name</td>
					</tr>
					<tr>
						<th scope='row' align='left'>CON COPIA:</th><td>$name_con_copia</td>
					</tr>
					<tr>
						<th scope='row' align='left'>ASUNTO:</th><td width='400px'>$asunto</td>
					</tr>
				</table>";	
	}	
		
		return $table;
	}
	
		
		
		
		
	 function _get_msg_email_no_aprobado($num_corresp,$depen_dest,$remit,$asunto,$dest_name,$name_con_copia,$id_historico,$status_desc)
	{
		
		if($name_con_copia=="")
		{
			$table="<div style='text-align:justify'>Usted ha recibido una correspondencia desde el sistema automatizado de envio de correspondencia del SATDC.
La misma puede ser vista y respondida desde el siguiente link: http://correspondencia.satdc.gob.ve</div><br><br><table>
					<tr>
						<th scope='row' width='270px' align='left'>NUMERO REFERENCIAL:</th><td>$id_historico</td>
					</tr>
					<tr>
						<th scope='row' align='left'>REMITENTE:</th><td>$remit</td>
					</tr>
					<tr>
						<th scope='row' align='left'>DEPENDENCIA DESTINATARIO:</th><td>$depen_dest</td>
					</tr>
					<tr>
						<th scope='row' align='left'>DESTINATARIO:</th><td>$dest_name</td>
					</tr>
					
					<tr>
						<th scope='row' align='left'>ASUNTO:</th><td width='400px'>$asunto</td>
					</tr>
				    <tr>
						<th scope='row' align='left'>STATUS:</th><td width='400px'>$status_desc</td>
					</tr>
				</table>";
	}
	else
	{
			$table="<div style='text-align:justify'>Usted ha recibido una correspondencia desde el sistema automatizado de envio de correspondencia del SATDC.
La misma puede ser vista y respondida desde el siguiente link: http://correspondencia.satdc.gob.ve</div><br><br><table>
					<tr>
						<th scope='row' width='270px' align='left'>NUMERO REFERENCIAL:</th><td>$id_historico</td>
					</tr>
					<tr>
						<th scope='row' align='left'>DEPENDENCIA REMITENTE:</th><td>$depen_remit</td>
					</tr>
					<tr>
						<th scope='row' align='left'>REMITENTE:</th><td>$remit</td>
					</tr>
					<tr>
						<th scope='row' align='left'>DESTINATARIO:</th><td>$dest_name</td>
					</tr>
					<tr>
						<th scope='row' align='left'>CON COPIA:</th><td>$name_con_copia</td>
					</tr>
					<tr>
						<th scope='row' align='left'>ASUNTO:</th><td width='400px'>$asunto</td>
					</tr>
					<tr>
						<th scope='row' align='left'>STATUS:</th><td width='400px'>$status_desc</td>
					</tr>
				</table>";	
	}	
			

		return $table;
	}	
}

/* End of file int_enviada.php */
/* Location: .application/controllers/int_enviada/int_enviada.php */