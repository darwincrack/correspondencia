<?php
class Reportes extends CI_Controller
{
	var $estatus=TRUE;
	/**
	*CONSTRUCTOR
	*/
	function __construct()
    {
       parent::__construct();
		$this->permiso_user_rol->validar_rol('ROL_CORRESP_INT_ENVIADA');
		$this->load->model('listas/listas'); 
		$this->load->model('reportes/interna_enviada'); 
		$this->load->library('encrypt');
		$this->load->helper('download');
		$this->load->helper('text');
		$this->load->helper('funciones');
		
    }
	
	/**
	* busca correspondecia enviadas filtrada por destinatarios
	*/
	function destinatario()
	{
		//$this->template->add_js('js/jquery/1.11.1/jquery.min.js');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery.ui.datepicker-es.js');
		//$this->template->add_js('js/funciones/funciones.js');
		$this->template->add_css('css/jquery-ui.css');
		
		
		
		
		if( $this->input->post('list_area_laboral') && $this->input->post('datepicker')&& $this->input->post('datepicker2') && $this->estatus===TRUE)
		{
			$data['item']= $this->interna_enviada->get_rpt_corresp_env_destinatario();
			if(empty($data['item']))
			{ 
				$data['error_db']='NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS';
			}
			else
			{
				$data['tabla'] = $this->load->view('reportes/int_enviada/table_destinatario', $data, TRUE);
			}
			$this->estatus=FALSE;
			
			
		}
		$data['list_area_laboral']= $this->listas->get_list_area_laboral();
		$this->template->write_view('content', 'reportes/int_enviada/destinatario',$data,TRUE);
		$this->template->write('title', 'Filtrado por Dependendia del Destinatario. <small>Interna Enviada</small>', TRUE);
		$this->template->render();	
	}
	
	
	
	
	
	
	function user_destinatario()
	{
		//$this->template->add_js('js/jquery/1.11.1/jquery.min.js');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery.ui.datepicker-es.js');
		//$this->template->add_js('js/funciones/funciones.js');
		$this->template->add_css('css/jquery-ui.css');
		
		
		
		
		if( $this->input->post('lista_func_destinatario') && $this->input->post('datepicker')&& $this->input->post('datepicker2') && $this->estatus===TRUE)
		{
			$data['item']= $this->interna_enviada->get_rpt_corresp_env_destinatario_user();
			if(empty($data['item']))
			{ 
				
				$data['error_db']=$this->session->userdata('id_area_laboral').'NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS';
			}
			else
			{
				$data['tabla'] = $this->load->view('reportes/int_enviada/table_destinatario', $data, TRUE);
			}
			$this->estatus=FALSE;
			
			
		}
		$data['lista_func_destinatario']= $this->listas->get_lista_func_destinatario();
		$this->template->write_view('content', 'reportes/int_enviada/destinatario_user',$data,TRUE);
		$this->template->write('title', 'Filtrado por Nombre de Destinatario. <small>Interna Enviada</small>', TRUE);
		$this->template->render();	
	}
	
	
	function int_env_view($id_corresp_int_env)
	{
		$this->template->add_js('js/funciones/readmore.js');
		if($id_corresp_int_env != '')
		{
			$data['item']= $this->interna_enviada->get_rpt_corresp_env_id(decrypt($id_corresp_int_env));
			echo $this->encrypt->decode($id_corresp_int_env);
		
			if(empty($data['item']))
			{ 
				$this->template->write('content', "<div class='error' style='text-align:center;'>NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS</div> ", TRUE);
			}
			else
			{
				$this->template->write('title', 'DETALLES. <small>Interna Enviada</small>', TRUE);
			 	$this->template->write_view('content', 'reportes/int_enviada/detalles_corresp',$data,TRUE);
			}
			$this->template->render();
		}
		else
		{
			show_404();
		}
		
	}
	
	
	/**
	* LISTA DE correspondencia por aprobar
	*/
	function status()
	{
		//jefe de area o sub area
		if($this->session->userdata('coord_sub_area')<>0 or  $this->session->userdata('coord_area')<>0)
		{
			$data['item']= $this->interna_enviada->get_rpt_corresp_env_status();
			if(empty($data['item']))
			{ 
				$this->template->write('content', 'No Data Display',TRUE);
			}
			else
			{
				$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
				$this->template->add_css('css/jquery-ui.css');
				$this->template->write_view('content', 'reportes/int_enviada/table_status_coord',$data,TRUE);
	
			}
				
				
			$this->template->write('title', 'Aprobar. <small>Interna Enviada</small>', TRUE);
		}
		else
		{
			$data['item']= $this->interna_enviada->get_rpt_corresp_env_status();
			if(empty($data['item']))
			{ 
				$this->template->write('content', 'No Data Display',TRUE);
			}
			else
			{
				$this->template->write_view('content', 'reportes/int_enviada/table_status',$data,TRUE);
	
			}
				
				
			$this->template->write('title', 'Status. <small>Interna Enviada</small>', TRUE);
				
			
			
		}
		
		$this->template->render();
		
		
		

	}
	
	
	
	/*
		function aprob_corresp()
	{
		$data['item']= $this->interna_enviada->get_rpt_corresp_env_status();
		if(empty($data['item']))
		{ 
			$this->template->write('content', 'No Data Display',TRUE);
		}
		else
		{
			$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
			$this->template->add_css('css/jquery-ui.css');
			$this->template->write_view('content', 'reportes/int_enviada/table_status_coord',$data,TRUE);

		}
			
			
		$this->template->write('title', 'Aprobar. <small>Interna Enviada</small>', TRUE);
		$this->template->render();	
	}
	
	
	*/
	
	
	



	 public function descargar_archivo($ruta,$doc)
	{	
		$ruta=rawurldecode(str_replace("~","/",$ruta));
		$doc=rawurldecode($doc);
		$path = $ruta.$doc;
		header('Content-Type: text/html; charset=utf-8');
 		header("Content-Description: Descargar");
 		header("Content-Disposition: attachment; filename=$doc");
 		header("Content-Type: application/force-download");
 		header("Content-Length: " . filesize($path));
 		header("Content-Transfer-Encoding: binary");
 		readfile($path);
	}
	

	
	/**
	* Valida los select tanto de dependencia destinatario, como de destinatario
	*
	* @access	private
	* @param	string
	* return 	boolean
	*/
	
	function validar_select($str)
	{
		if ($str == 'seleccione' or $str==FALSE)
		{
			$this->form_validation->set_message('validar_select','Debe selecionar una opcion');
			return FALSE;
		}
		return TRUE;
	}	
}

/* End of file reportes.php */
/* Location: .application/controllers/int_enviada/reportes.php */