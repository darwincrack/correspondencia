<?php class Reportes extends CI_Controller{
var $estatus=TRUE;
	function __construct()
    {
       parent::__construct();
		$this->permiso_user_rol->validar_rol('ROL_REPORTES_GENERAL');
		$this->load->library("mpdf");
		$this->load->model('listas/listas'); 
		$this->load->model('reportes/int_general');
		$this->load->model('reportes/ext_general');
		$this->load->helper('funciones');	
    }
	// menu interna
	function index()
	{
	
	  $this->template->write('title', 'Reporte General.<small>Interna</small>', TRUE);
	  $this->template->write_view('content', 'reportes/general/menu_interna');
	  $this->template->render();	

	}
	//menu externa 
	function externa(){
	  $this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
	  $this->template->add_css('css/jquery-ui.css');
	  $this->template->write('title', 'Reporte General.<small>Externa</small>', TRUE);
	  $this->template->write_view('content', 'reportes/general/menu_externa');
	  $this->template->render();	

	}
	//1
	function rpt_full_env()
	{
		$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery.ui.datepicker-es.js');
		$this->template->add_css('css/jquery-ui.css');
		
		if( $this->input->post('list_area_laboral') && $this->estatus===TRUE && $this->input->post('datepicker')&& $this->input->post('datepicker2') )
			{
				$data['item']= $this->int_general->get_rpt_corresp_int_env_gen();
				if(empty($data['item']))
				{ 
				  $data['error_db']='NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS';
				  $data['tabla']='';
				  
				}
				else
				{
				  $data['tabla'] = $this->load->view('reportes/general/rpt_table', $data, TRUE);
				  
				}
				$this->estatus=FALSE;
			}
			
		  	 $data['list_area_laboral']= $this->listas->get_list_area_laboral();
			 $data['title_label']='Enviado por:';
		  	 $this->template->write_view('content', 'reportes/general/rpt_full_int_env',$data,TRUE);
		 	 $this->template->write('title', 'CORRESPONDENCIAS INTERNAS ENVIADAS', TRUE);
		 	 $this->template->render();	
		
	}
	
	//2
	function rpt_func_remit()
	{
		$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery.ui.datepicker-es.js');
		$this->template->add_css('css/jquery-ui.css');
				
		if( $this->input->post('list_area_laboral') && $this->estatus===TRUE && $this->input->post('list_funcionario'))
			{
				$data['item']= $this->int_general->get_rpt_corresp_env_remitente_user_gen();
				if(empty($data['item']))
				{ 
				  $data['error_db']='NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS';
				  $data['tabla']='';
				  
				}
				else
				{
				  $data['tabla'] = $this->load->view('reportes/general/rpt_table', $data, TRUE);
				  
				}
				$this->estatus=FALSE;
			}
			
		  	 $data['list_area_laboral']= $this->listas->get_list_area_laboral();
		  	 $this->template->write_view('content', 'reportes/general/rpt_remit_int',$data,TRUE);
		 	 $this->template->write('title', 'CORRESPONDENCIAS INTERNAS REMITENTE', TRUE);
		 	 $this->template->render();	
			
	}
	
	//3
	function rpt_full_recib()
	{
		
		$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery.ui.datepicker-es.js');
		$this->template->add_css('css/jquery-ui.css');
			
		if( $this->input->post('list_area_laboral') && $this->estatus===TRUE && $this->input->post('datepicker')&& $this->input->post('datepicker2') )
			{
				$data['item']= $this->int_general->get_rpt_corresp_int_recib_gen();
				if(empty($data['item']))
				{ 
				  $data['error_db']='NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS';
				  $data['tabla']='';
				  
				}
				else
				{
				  $data['tabla'] = $this->load->view('reportes/general/table_rpt_full_int_recib', $data, TRUE);
				  
				}
				$this->estatus=FALSE;
			}
			
		  	 $data['list_area_laboral']= $this->listas->get_list_area_laboral();
			 $data['title_label']='Recibido por:';
		  	 $this->template->write_view('content', 'reportes/general/rpt_full_int_recib',$data,TRUE);
		 	 $this->template->write('title', 'CORRESPONDENCIAS INTERNAS RECIBIDAS', TRUE);
		 	 $this->template->render();	
		
	}
	
	
	//4
	function num_corresp()
	{
		$data[]='';
		$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery.ui.datepicker-es.js');
		$this->template->add_css('css/jquery-ui.css');
					
		if( $this->input->post('list_area_laboral') && $this->input->post('periodo') && $this->estatus===TRUE && $this->input->post('num_corresp'))
			{
				if( $this->input->post('list_sub_area_laboral')=="0") // si es 0 no se selecciono sub area
				{
					
					
					$data['item']= $this->int_general->get_rpt_corresp_int_num_corresp();
				}
				else 
				{
				
					$data['item']= $this->int_general->get_rpt_corresp_int_num_corresp_sub_area();
				}
				
				if(empty($data['item']))
				{ 
				  $data['error_db']='NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS';
				  $data['tabla']=''; 
				}
				else
				{
				  $data['tabla'] = $this->load->view('reportes/general/rpt_table', $data, TRUE);
				  
				}
				$this->estatus=FALSE;
			}

			
		  	 $data['list_area_laboral']= $this->listas->get_list_area_laboral();
			 $data['title_label']='Departamento:';
		  	 $this->template->write_view('content', 'reportes/general/num_corresp',$data,TRUE);
		 	 $this->template->write('title', 'NUMERO DE CORRESPONDENCIA', TRUE);
		 	 $this->template->render();	
		
		
		
	}

//externas 

	//1
	function rpt_full_ext_env()
	{
		
		$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery.ui.datepicker-es.js');
		$this->template->add_css('css/jquery-ui.css');
		
		if( $this->input->post('list_area_laboral') && $this->estatus===TRUE && $this->input->post('datepicker')&& $this->input->post('datepicker2') )
			{
				$data['item']= $this->ext_general->get_rpt_corresp_ext_env_gen();
				if(empty($data['item']))
				{ 
				  $data['error_db']='NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS';
				  $data['tabla']='';
				  
				}
				else
				{
				  $data['tabla'] = $this->load->view('reportes/general/rpt_table_ext_env', $data, TRUE);
				  
				}
				$this->estatus=FALSE;
			}
			
		  	 $data['list_area_laboral']= $this->listas->get_list_area_laboral();
			 $data['title_label']='EnSviado por:';
			 $this->template->write('title', 'CORRESPONDENCIAS EXTERNAS ENVIADAS', TRUE);
		  	 $this->template->write_view('content', 'reportes/general/rpt_full_ext_env',$data,TRUE);
		 	 $this->template->render();	
		
	}
	
	
	
	//2 por ente o persona destinatario
	function rpt_full_destinatario()
	{
			
			$data['msg_10']='';
			if($this->estatus===TRUE && $this->input->post('ente_persona_dest'))
			{
				$data['item']= $this->ext_general->get_rpt_corresp_ext_env_dest_gen();
				if(empty($data['item']))
				{ 
				  $data['error_db']='NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS';
				  $data['tabla']='';
				  
				}
				else
				{
				  $this->load->helper('text');
				  $data['tabla'] = $this->load->view('reportes/general/rpt_table_dest_ext_env', $data, TRUE);
				  
				}
			$this->estatus=FALSE;
			}
			
		  	 $this->template->write_view('content', 'reportes/general/rpt_full_dest_ext_env',$data,TRUE);
		 	 $this->template->write('title', 'Filtrado por Ente o Persona Destinatario2. <small>Externa Enviada</small>', TRUE);
		 	 $this->template->render();	
	
	}
	
	
	/**
	* Obtiene de la BD la lista de funcionario, dependiendo el area laboral(llena el select)
	* @access	public
	* return 	void
	*/
	
	
	public function get_funcionarios()
	{
        if($this->input->post('id_area_laboral'))
        {
            $id_area_laboral = $this->input->post('id_area_laboral');
            $funcionario= $this->listas->get_lista_funcionarios($id_area_laboral);
			echo "<select name='list_funcionario' id='list_funcionario'>";
            foreach($funcionario as $item_funcionario):
            ?>
             <option value="<?php echo $item_funcionario['funcionario_id'] ?>"><?php echo  $item_funcionario['nombre_funcionario'] ?></option>
            <?php
           endforeach;
		   echo "</select>";
        }
	}	
	
	
	//interna 
	function detalles_view($id_corresp_int=FALSE)
	{
		$this->template->add_js('js/funciones/readmore.js');
		if($id_corresp_int != '')
		{
			
			
			$data['item']= $this->int_general->get_rpt_corresp_int_gen_id(decrypt($id_corresp_int));
		
			if(empty($data['item']))
			{ 
				$this->template->write('content', "<div class='error' style='text-align:center;'>NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS</div> ", TRUE);
			}
			else
			{
				$this->template->write('title', 'DETALLES <small>Reporte General</small>', TRUE);
			 	$this->template->write_view('content', 'reportes/general/detalles_corresp',$data,TRUE);
			}
			$this->template->render();
		}
		else
		{
			show_404();
		}
	
	}
	
	//externa
	function detalles_view_ext($id_corresp_ext=FALSE)
	{
		if($id_corresp_ext != '')
		{
			
			
			$data['item']= $this->ext_general->get_rpt_corresp_ext_gen_id(decrypt($id_corresp_ext));
		
			if(empty($data['item']))
			{ 
				$this->template->write('content', "<div class='error' style='text-align:center;'>NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS</div> ", TRUE);
			}
			else
			{
				$this->template->write('title', 'DETALLES <small>Reporte General</small>', TRUE);
			 	$this->template->write_view('content', 'reportes/general/detalles_corresp_ext',$data,TRUE);
			}
			$this->template->render();
		}
		else
		{
			show_404();
		}
	
	}
	
	public function descargar_archivo($ruta,$doc)
	{	
		$ruta=rawurldecode(str_replace("~","/",$ruta));
		$doc=rawurldecode($doc);
		$path = $ruta.$doc;
		header('Content-Type: text/html; charset=utf-8');
 		header("Content-Description: Descargar");
 		header("Content-Disposition: attachment; filename=$doc");
 		header("Content-Type: application/force-download");
 		header("Content-Length: " . filesize($path));
 		header("Content-Transfer-Encoding: binary");
 		readfile($path);
	}
	
		//el pdf final con la correspondencia ya guardada
	function pdf($corresp_id)
	{
		$item=$this->int_general->get_rpt_corresp_int_gen_id(decrypt($corresp_id));
		
		if(! empty($item))
		{
		
		foreach ($item as $item_new):
			        //CONTENIDO DEL PDF
		$data['num_corresp']	=	$item_new['num_corresp_int'];
		$data['destinatario']	=	$item_new['name_destinatario'];
		$data['dest_cargo']		=	$item_new['cargo_destinatario'];
		$data['remitente']		=	$item_new['name_remitente'];
		$data['remit_cargo']	=	$item_new['cargo_remitente'];
		$data['asunto']			=	$item_new['asunto'];
		$data['fecha_envio']	=	$item_new['fecha_enviado_pdf'];
		$data['contenido']		=	$item_new['contenido'];
		$data['name_con_copia']	=	$item_new['name_copia'];
		$data['cargo_con_copia']=	$item_new['cargo_copia'];
		$data['status']			=	$item_new['id_status'];
		$status_corresp			=	$item_new['id_status'];
		// FIN CONTENIDO DEL PDF
		endforeach;
		
		
		$this->mpdf->mPDF('','letter','','',20,20,40,30,'L');
		$this->mpdf->SetDisplayMode(60);
		//$this->mpdf->mPDF('','letter','','',20,20,MT,MB,'L');
		$css_ruta=base_url().'css/pdf.css';
		$css = file_get_contents($css_ruta);
		$this->mpdf->WriteHTML($css,1);
		
		if($status_corresp<>"3")
		{
			$this->mpdf->SetWatermarkText('POR APROBAR');
			$this->mpdf->showWatermarkText = true;
			$this->mpdf->SetDisplayMode(60);
		}
		$html = $this->load->view('pdf/int_enviada_pdf', $data, true);
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output();
		}
		else
		{
			show_404();
		}
	}
		
}
/* End of file reportes.php */
/* Location: .application/controllers/reporte_general/reportes.php */