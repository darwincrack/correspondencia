<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportes extends CI_Controller{

	var $estatus=TRUE;
	function __construct()
    {
       parent::__construct();
		$this->permiso_user_rol->validar_rol('ROL_CORRESP_EXT_ENVIADA');
		$this->load->model('reportes/externa_enviada');
		$this->load->helper('funciones');
    }
	
	//fecha_envio
	function fecha_envio()
	{
		
		$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery.ui.datepicker-es.js');
		$this->template->add_css('css/jquery-ui.css');
		
			
			$data['msg_10']='';
			if($this->estatus===TRUE && $this->input->post('datepicker')&& $this->input->post('datepicker2') )
			{
				$data['item']= $this->externa_enviada->get_rpt_corresp_ext_env_fecha();
				if(empty($data['item']))
				{ 
				  $data['error_db']='NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS';
				  $data['tabla']='';
				  
				}
				else
				{
				  $data['ubicacion']=1;
				  $data['tabla'] = $this->load->view('reportes/ext_enviada/table2', $data, TRUE);
				  
				}
			$this->estatus=FALSE;
			}
			
		  	 $this->template->write_view('content', 'reportes/ext_enviada/fecha_envio2',$data,TRUE);
		 	 $this->template->write('title', 'Filtrado por Fecha de Envio. <small>Externa Enviada</small>', TRUE);
		 	 $this->template->render();	
	
	}

	//por ente o persona destinatario
	function destinatario()
	{
			
			$data['msg_10']='';
			if($this->estatus===TRUE && $this->input->post('ente_persona_dest'))
			{
				$data['item']= $this->externa_enviada->get_rpt_corresp_ext_env_dest();
				if(empty($data['item']))
				{ 
				  $data['error_db']='NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS';
				  $data['tabla']='';
				  
				}
				else
				{
				  $this->load->helper('text');
				  $data['ubicacion']=2;
				  $data['tabla'] = $this->load->view('reportes/ext_enviada/table2', $data, TRUE);
				  
				}
			$this->estatus=FALSE;
			}
			
		  	 $this->template->write_view('content', 'reportes/ext_enviada/destinatario',$data,TRUE);
		 	 $this->template->write('title', 'Filtrado por Ente o Persona Destinatario. <small>Externa Enviada</small>', TRUE);
		 	 $this->template->render();	
	
	}

	
	//detalles correspondencia
	function view($id_corresp_ext_env=FALSE)
	{
		if($id_corresp_ext_env != '')
		{
			$data['item']= $this->externa_enviada->get_rpt_corresp_ext_enviada_id(decrypt($id_corresp_ext_env));
		
			if(empty($data['item']))
			{ 
				$this->template->write('content', "<div class='error' style='text-align:center;'>NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS</div> ", TRUE);
			}
			else
			{	$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
				$this->template->add_css('css/jquery-ui.css');
				$this->template->write('title', 'DETALLES <small>Externa Enviada</small>', TRUE);
			 	$this->template->write_view('content', 'reportes/ext_enviada/detalles_corresp2',$data,TRUE);
			}
			$this->template->render();
		}
		else
		{
			show_404();

		}
	}
}


/* End of file ext_recibida.php */
/* Location: ./application/controllers/ext_enviada/reportes.php */