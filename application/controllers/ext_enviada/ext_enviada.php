<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ext_enviada extends CI_Controller{
	
	var $status_error_db 	= 	FALSE;	// estatus a la hora de interactuar con la base de datos
	var $msg_error_db 		= 	'';		// mensajes de errores en los insert o update en la base de datos
	var $estatus=TRUE;
	
	function __construct()
    {
       parent::__construct();
		$this->permiso_user_rol->validar_rol('ROL_CORRESP_EXT_ENVIADA');
		$this->load->model('listas/listas');
		$this->load->helper('funciones');
    }
	
	
	function index()
	{
		
		$this->template->add_css('css/jquery-ui.css');
		$this->template->add_css('css/autocomplete.css');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
		
		$data['error_db']=$this->msg_error_db;
		$this->form_validation->set_rules('ubic_recep', 'Lista de Salida', 'callback_validar_select');
		$this->form_validation->set_rules('depen_destinatario', 'Depen. Destinatario', 'required|trim||max_length[100]|xss_clean');
		$this->form_validation->set_rules('destinatario', 'Destinatario', 'required|trim||max_length[100]|xss_clean');
		$this->form_validation->set_rules('asunto', 'Asunto', 'required|trim||max_length[250]|xss_clean');
		$this->form_validation->set_rules('contenido', 'Contenido', 'trim||max_length[2000]|xss_clean');
		$this->form_validation->set_message('required', ' %s No puede estar en blanco');
		$this->form_validation->set_error_delimiters('<div class="error">','</div>');
		if ($this->form_validation->run() === FALSE or $this->status_error_db===TRUE)
		{
			$data['list_funcionario']= $this->listas->get_lista_funcionarios($this->session->userdata('id_area_laboral'));
			$data['list_recep']= $this->listas->get_lista_recepcion();
			$this->template->write('title', 'Redactar Correspondencia. <small>Externa Enviada</small>', TRUE);
			$this->template->write_view('content', 'ext_enviada/ext_enviada',$data,TRUE);
			$this->template->render();
		}
		else
		{
			$this->load->model('ext_enviada/ext_enviada_model');
			$ip	 =	getRealIP();
			$estatus=$this->ext_enviada_model->set_ext_enviada($ip);
			if($estatus->estatus==1)
			{
				$this->msg_error_db=1; //exito
			}
			else
			{
				$this->msg_error_db=2; //error
			}
				$this->status_error_db=TRUE;
				$this->index();
			
		}
	}


	//autocomplete empresas externas
	function get_entes_externos() 
	{ 
   		$match = $this->input->get('term', TRUE);  // TRUE para hacer un filtrado XSS  
   		$entes_json= $this->listas->get_entes_externos($match);
   		echo $entes_json; // se regresa en formato json
	}
	
	//autocomplete personas externas
	function get_personas_externas() 
	{ 
   		$match = $this->input->get('term', TRUE);  // TRUE para hacer un filtrado XSS
		$ente_externo = $this->input->get('ente_externo', TRUE);  // TRUE para hacer un filtrado XSS  
   		$entes_json= $this->listas->get_personas_externas($match,$ente_externo);
   		echo $entes_json; // se regresa en formato json
	}
	
	
		/**
	* Valida los select 
	*
	* @access	private
	* @param	string
	* return 	boolean
	*/
	
	function validar_select($str)
	{
		
		if ($str == 'seleccione' or $str==FALSE)
		{
			$this->form_validation->set_message('validar_select','Debe selecionar una opcion');
			return FALSE;
		}
		return TRUE;
	}
		
}

/* End of file ext_enviada.php */
/* Location: ./application/controllers/ext_enviada/ext_enviada.php */