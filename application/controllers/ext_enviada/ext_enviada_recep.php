<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ext_enviada_recep extends CI_Controller{

	var $estatus=TRUE;

	function __construct()
    {
       parent::__construct();
		$this->permiso_user_rol->validar_rol('ROL_CORRESP_EXT_ENVIADA_RECEP');
		$this->load->model('reportes/externa_enviada');
		$this->load->helper('funciones');
		
    }
	
	function index()
	{
		$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery.ui.datepicker-es.js');
		$this->template->add_css('css/jquery-ui.css');
		
		
			if($this->estatus===TRUE)
			{
				
				//se obtiene las diez ultimas correspondencias
				
				$data['item']= $this->externa_enviada->get_rpt_corresp_ext_enviada_recep_teen();
				if(empty($data['item']))
				{ 
					$data['error_db']='NADIE HA ENVIADO CORRESPONDENCIA';
				}
				else
				{
					$data['tabla'] = $this->load->view('reportes/ext_enviada/table', $data, TRUE);
					$data['msg_10'] = "<div class='top_diez'><span>Ultimas 10 Correspondencias</span></div>";	
					
				}	
			}
			
			
			if($this->estatus===TRUE && $this->input->post('datepicker')&& $this->input->post('datepicker2') )
			{
				$data['item']= $this->externa_enviada->get_rpt_corresp_ext_enviada_recep_fecha();
				if(empty($data['item']))
				{ 
				  $data['error_db']='NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS';
				  $data['tabla']='';
				  
				}
				else
				{
				  $data['tabla'] = $this->load->view('reportes/ext_enviada/table', $data, TRUE);
				  
				}
			$data['msg_10']='';
			$this->estatus=FALSE;
			}
			
		  	 $this->template->write_view('content', 'reportes/ext_enviada/fecha_envio',$data,TRUE);
		 	 $this->template->write('title', 'CORRESPONDENCIA DE SALIDA <small>Externa Enviada</small>', TRUE);
		 	 $this->template->render();	
	}
	
	
	
	//detalles correspondencia
	function view($id_corresp_ext_env=FALSE)
	{
		if($id_corresp_ext_env != '')
		{
			$data['item']= $this->externa_enviada->get_rpt_corresp_ext_enviada_recep_id(decrypt($id_corresp_ext_env));
		
			if(empty($data['item']))
			{ 
				$this->template->write('content', "<div class='error' style='text-align:center;'>NO SE HA ENCONTRADO INFORMACION EN LA BASE DE DATOS</div> ", TRUE);
			}
			else
			{	$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
				$this->template->add_css('css/jquery-ui.css');
				$this->template->write('title', 'DETALLES <small>Externa Enviada</small>', TRUE);
			 	$this->template->write_view('content', 'reportes/ext_enviada/detalles_corresp',$data,TRUE);
			}
			$this->template->render();
		}
		else
		{
			show_404();

		}
	}
	
	
	function add_correlativo()
	{

		if($this->input->post('num_correlativo') && $this->input->post('id_corresp_ext'))
		{
			$id_corresp_ext=decrypt($this->input->post('id_corresp_ext'));
			$num_correlativo=$this->input->post('num_correlativo');	
			if(ctype_digit($num_correlativo)){
				$this->load->model('ext_enviada/ext_enviada_model');		
				$data=$this->ext_enviada_model->set_update_corresp_ext_correlativo($id_corresp_ext,$num_correlativo);
				$estatus= $data->estatus;
			}
			else
			{
				$estatus=0;
			}
			echo $estatus;
			
		}
		
	}
		
}


/* End of file ext_enviada_recep.php */
/* Location: ./application/controllers/ext_enviada/ext_enviada_recep.php */