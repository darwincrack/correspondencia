<?php 
class Login extends CI_Controller
{
	function __construct()
    {
       parent::__construct();
       $this->load->model('login/login_user');
	   $this->load->library('encrypt');
	   $this->load->helper('cookie');
	   $this->load->helper('url');
    }
	
	function index()
	{
		//ejemplo
	//$this->template->write_view('content', 'registro/formulario_registro', $data, TRUE);
	//$this->template->add_css('css/styles.css');
	//$this->template->write_view('header', 'menu/menu');
	//$this->template->render();
	 
	if ($this->session->userdata('is_logued_in') ===FALSE)
	{
		$data['token'] = $this->_token();
		$this->template->write_view('content','login/login',$data,TRUE);
	}
	else
	{
		$this->_get_rol_user();
	}
	 $this->template->render();
	}
	
	
	public function user_login()
	{
		if($this->input->post('token',true) && $this->input->post('token',true) == $this->session->userdata('token'))
		{
        	$this->form_validation->set_rules('usuario', 'Usuario', 'required|trim|min_length[2]|max_length[20]|xss_clean');
            $this->form_validation->set_rules('contrasena', 'Contraseña', 'required|trim|min_length[3]|max_length[20]|xss_clean');
			$this->form_validation->set_message('required', ' %s No puede estar en blanco');
			$this->form_validation->set_message('min_length', ' El campo %s debe ser al menos de %s caracteres');
			$this->form_validation->set_message('max_length', ' El campo %s no puede exceder de %s caracteres');
			$this->form_validation->set_error_delimiters('<div class="error">','</div>');
            //lanzamos mensajes de error si es que los hay
            
			if($this->form_validation->run() === FALSE)
			{
				$this->index();
			}else{
				$check_user = $this->login_user->get_login_user();
				if($check_user == TRUE)
				{
					$data = array(
	                'is_logued_in' 		=> 		TRUE,
	                'id_usuario' 		=> 		$check_user->usuario_id,
	                'user_login'		=>		$check_user->login,
	                'username' 			=> 		$check_user->name_user,
					'id_area_laboral' 	=> 		$check_user->id_area_laboral,
					'funcionario_id' 	=> 		$check_user->funcionario_id,
					'coord_area'		=>		$check_user->coord_area, //1 si es coord, 0 si no lo es
					'coord_sub_area'	=>		$check_user->coord_sub_area //1 si es coord, 0 si no lo es
            		);		
					$this->session->set_userdata($data);
					$this->index();
				}
			}
		}else{
			delete_cookie("men_corresp");
			redirect(base_url().'login');
		}
	}
	
	public function _get_rol_user()
	{
		$data['rol_user']= $this->login_user->get_rol_user();
		$this->session->set_userdata('roles', $data['rol_user']);
		$this->template->write_view('header','menu/menu',$data,TRUE);
	}
	
	
	public function _token()
	{
		$token = md5(uniqid(rand(),true));
		$this->session->set_userdata('token',$token);
		return $token;
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		delete_cookie("men_corresp");
		redirect(base_url().'login');
	}
}