<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ext_Recibida extends CI_Controller{

	var $status_error_db 	= 	FALSE;	// estatus a la hora de interactuar con la base de datos
	var $msg_error_db 		= 	'';		// mensajes de errores en los insert o update en la base de datos
	var $estatus=TRUE;

	function __construct()
    {
       parent::__construct();
		$this->permiso_user_rol->validar_rol('ROL_CORRESP_EXT_RECIBIDA');
		$this->load->model('listas/listas');
		$this->load->helper('funciones');
    }
	
	function index()
	{
				
		$this->template->add_css('css/jquery-ui.css');
		$this->template->add_css('css/autocomplete.css');
		$this->template->add_js('js/jquery-ui/1.10.3/jquery-ui.js');
		$data['error_db']=$this->msg_error_db;
		$this->form_validation->set_rules('num_corresp', 'Num Corresp', 'required|trim||max_length[20]|xss_clean');
		$this->form_validation->set_rules('depen_remitente', 'Depend. Remitente', 'required|trim||max_length[100]|xss_clean');
		$this->form_validation->set_rules('remitente', 'Remitente', 'required|trim||max_length[100]|xss_clean');
		$this->form_validation->set_rules('list_area_laboral', 'Lista Destinatarios', 'callback_validar_select');
		$this->form_validation->set_rules('list_funcionario', 'Lista Funcionarios', 'callback_validar_select');
		$this->form_validation->set_rules('asunto', 'Asunto', 'required|trim||max_length[250]|xss_clean');
		$this->form_validation->set_rules('datepicker', 'Fecha', 'required|trim||max_length[20]|xss_clean');
		$this->form_validation->set_message('required', ' %s No puede estar en blanco');
		$this->form_validation->set_message('max_length', ' El campo %s no puede exceder de %s caracteres');
		$this->form_validation->set_error_delimiters('<div class="error">','</div>');
		
		if ($this->form_validation->run() === FALSE or $this->status_error_db===TRUE)
		{
			$data['list_area_laboral']= $this->listas->get_list_area_laboral();
			$this->template->write('title', 'Redactar Correspondencia. <small>Externa Recibida</small>', TRUE);
			$this->template->write_view('content', 'ext_recibida/ext_recibida',$data,TRUE);
			$this->template->render();
		}
		else
		{
			$this->load->model('ext_recibida/ext_recibida_model');
			$ip	 =	getRealIP();
			$estatus=$this->ext_recibida_model->set_ext_recibida($ip);
			
			if($estatus->estatus==1)
			{
				$this->msg_error_db=1; //exito
			}
			else
			{
				$this->msg_error_db=2; //error
			}
				$this->status_error_db=TRUE;
				$this->index();
		}
	}
	
	/**
	* Valida los select tanto de dependencia destinatario, como de destinatario
	*
	* @access	private
	* @param	string
	* return 	boolean
	*/
	
	function validar_select($str)
	{
		
		if ($str == 'seleccione' or $str==FALSE)
		{
			$this->form_validation->set_message('validar_select','Debe selecionar una opcion');
			return FALSE;
		}
		return TRUE;
	}
}

/* End of file ext_recibida.php */
/* Location: ./application/controllers/ext_recibida/ext_recibida.php */