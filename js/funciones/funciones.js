// JavaScript Document

//mantener el scroll

 $(document).ready(function(){ //inicio jquery
	//ver mas
if ($('#table_correo').length){
 $(function() {
 $("#table_correo").readmore({
  speed: 5,
  maxHeight: 143
});	
});
}

if ($('#table_correo2').length){
 $(function() {
 $("#table_correo2").readmore({
  speed: 5,
  maxHeight: 133
});	
});
}

  //textarea
if (typeof(tinymce) !=="undefined") {
 $(function() {
 tinymce.init({
    selector: "textarea",
    theme: "modern",
	language : "es",
	width : 810,
	height : 300,
	browser_spellcheck : true,
	/*
	corrector ortografico en prueba
	external_plugins: {"nanospell": "nanospell/plugin.js"},
	nanospell_server: "php",
	nanospell_dictionary: "es",*/
     plugins: [
                " advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table directionality emoticons template textcolor paste textcolor   colorpicker textpattern "
        		],

        toolbar1: "  newdocument undo redo  |  bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | forecolor backcolor table  bullist | numlist| outdent indent    | fontsizeselect ",
  menubar: false,
  
});	
  });
}
 //deshabilitar click derecho
 $("#num_correlativo").bind("contextmenu",function(e){
        return false;
    });
  //solo nuymeros
  $("#num_correlativo").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
	
	
 //abrir_dialog_correlativo
 $("#btn_dialog").click(function(){
   	$("#dialog").dialog({
   	modal: true,
   	title: "Numero de Correlativo",
   	height:180,
   	width: 400,
   	maxWidth: 600,
   	show: "fold",
   	hide: "scale"
	});
  });
 
 
 //agregar numero de correlativo
  $("#btn_correlativo").click(function(){
	  $("#msg_corre").html('Espere...');
	  $.post("../add_correlativo/", {
		  id_corresp_ext : $('#id_corresp_ext').val(),
		  num_correlativo : $('#num_correlativo').val()
	  }, function(data) {
		  
		  if(data==0)
		  {
		  	  $("#msg_corre").html('Correlativo Invalido');
		  }
		  else
		  {
			  location.reload(true);
		  }  
	  });
  });   
$(window).scroll(function()
            {
                if ($(this).scrollTop() > 200){
					 $('#nav').addClass("fixed").fadeIn();
					 $('#seccion').addClass("margen").fadeIn();
				}
                else {
					$('#nav').removeClass("fixed");
					$('#seccion').removeClass("margen");
					
				}
            });


//select funcionarios por departamento selecionado

		$("#list_area_laboral").change(function() {
			$("#list_area_laboral option:selected").each(function() {
			var url="";
			var location	   =	$('#location').val();
			var id_area_laboral = $('#list_area_laboral').val();
			 
			 if ($('#list_con_copia').length>=1)
			 {
				$('#content_con_copia').empty();
				$("#text_copia").css("display","none");
			 }
			 
			 
			 switch(location) 
			{
			  case "2":
				  url="../../../get_funcionarios";
				  break;
			  default:
				url="../../int_enviada/int_enviada/get_funcionarios";
			}
			 
			 
			 
			 
				
				
				$.post(url, {
					id_area_laboral : id_area_laboral
				}, function(data) {
					$("#list_funcionario").html(data);
					
					if($("#list_area_laboral option:selected").val() != 11)
					{
						
						$('input[name=confidencial]').attr("disabled", false);
						
						if ($('div#add_con_copia').length==0)
						{
							$("#text_copia").css("display","inline");
						}
					
					}
					else
					{
						$("#text_copia").css("display","none");
						$('input[name=confidencial]').attr("disabled", true);
						$('input[name=confidencial]').prop('checked', false);
					}
					
					 
					
					
					
					
				});
			});
		})



		
		
//VERIFICA Y MUESTRA SI SE DEBE UTILIZAR LA SUB AREA DEL FUNCIONARIO	
		$("#list_funcionario_remitente").change(function() {
			
			$("#list_funcionario_remitente option:selected").each(function() {
				$( "#add_sub_area" ).css('display','block');
				$( "#add_sub_area" ).html("Espere...");
				var id_funcionario = 	$('#list_funcionario_remitente').val();
				var location	   =	$('#location').val();
				
				
				
					switch(location) 
					{
						case "1":
							url="../../../../../../get_sub_area_laboral_func/";
							break;
						case "2":
								url="../../../get_sub_area_laboral_func";
							break;
						default:
						   url="../../int_enviada/int_enviada/get_sub_area_laboral_func";
					}
	
				
				
				
				/*if(location !=1)
				{
					url="../../int_enviada/int_enviada/get_sub_area_laboral_func";
				}
				else
				{
					url="../../../../../../get_sub_area_laboral_func/";
				}*/
				
				
				
				$.post(url, {
					id_funcionario : id_funcionario
				}, function(data) {
					if(data=='404')
					{
						$( "#add_sub_area" ).css('display','none');
					}
					else
					{
						$("#add_sub_area").html(data);
					}
				});
			});
		})
	
///////////////////////////////////////////	
	
//el input de fecha
if ($('#datepicker').length){
$(function() {
   $.datepicker.setDefaults($.datepicker.regional["es"]);
$("#datepicker").datepicker({
	changeMonth: true, //muestra para seleccionar el mes, en el calendario
	changeYear: true,
	dateFormat: 'yy-mm-dd'
});
  });
}


 if ($('#datepicker2').length){
$(function() {
   $.datepicker.setDefaults($.datepicker.regional["es"]);
$("#datepicker2").datepicker({
	changeMonth: true, //muestra para seleccionar el mes, en el calendario
	changeYear: true,
	dateFormat: 'yy-mm-dd'
});
  });
}
  
 
 //select funcionarios por departamento selecionado

		$("#list_area_laboral2").change(function() {
			$("#list_area_laboral2 option:selected").each(function() {
				var id_area_laboral = $('#list_area_laboral2').val();
				if (id_area_laboral.length>0){
					$.post("../../reporte_general/reportes/get_funcionarios", {
					id_area_laboral : id_area_laboral
				}, function(data) {
						$("#content_select").html('espero...');
						$("#content_select").html(data);
					});
				}
			});
		})
	

//autocompletar empresa
 if ($('#entes_autocomplete').length){
  $(function() {
 $('#entes_autocomplete').autocomplete({
	minLength: 3,
    source: '../ext_enviada/ext_enviada/get_entes_externos'
  });
 });
 }
 
//autocompletar nombre persona
if ($('#personas_autocomplete').length){
$(function() {
$('#personas_autocomplete').autocomplete({
            source: function(request, response) {
            $.ajax({
            url: "../ext_enviada/ext_enviada/get_personas_externas",
            dataType: "json",
            data: {
              term : request.term,
              ente_externo : $("#entes_autocomplete").val()
             },
            success: function(data) {
                 response(data);
                  }
                  });
                  },
                  minLength: 3
             });
  });
 }
 
 //accordion
if ($('#accordion').length){
   $(function() {
    $( "#accordion" ).accordion();
  });
}



//vista previa PDF
$("#btn_vista_previa").click(function(){
	
	$('#loading').css("display","block");
	tinyMCE.triggerSave();
	var id_remitente	=	$('#list_funcionario_remitente').val();
	var id_destinatario	=	$('#list_funcionario').val();
	var asunto			=	$('#asunto').val();
	var contenido		=	$('#text_obser').val();
	var respuesta		=	$('#respuesta').val();
	var location		=	$('#location').val();
	var dest_con_copia	=	$('#list_con_copia').val();
	var url				=	'';
	
	
	switch(location) 
	{
		case "1":
			url="../../../../../../../create_temp_pdf/";
			break;
		case "2":
			url="../../../create_temp_pdf";
			break;
		default:
		   url="../../int_enviada/int_enviada/create_temp_pdf";
	}
	
	
	
	/*if(location !=1)
	{
		url="../../int_enviada/int_enviada/create_temp_pdf";
	}
	else if(location==2)
	{
		url="../../../int_enviada/int_enviada/get_sub_area_laboral_func";
	}
	else
	{
		url="../../../../../../../create_temp_pdf/";
	}*/
	
  $.ajax({
  url :url, 
  type:"POST",
  data: { id_remitente: id_remitente, id_destinatario : id_destinatario, asunto : asunto, contenido : contenido, respuesta:respuesta, dest_con_copia: dest_con_copia},
  success:function(result){
	var pdf=result;
	$('#loading').css("display","none");
	
		switch(location) 
	{
		case "1":
			window.open('../../../../../../../preview/'+pdf, "", "width=800, height=500,left=300");
			break;
		case "2":
			window.open('../../../preview/'+pdf, "", "width=800, height=500,left=300");
			break;
		default:
		  window.open('../../int_enviada/int_enviada/preview/'+pdf, "", "width=800, height=500,left=300");
	}
	
	
	
	
	/*if(location!=1)
	{
		window.open('../../int_enviada/int_enviada/preview/'+pdf, "", "width=800, height=500,left=300");
	}
	else
	{
		window.open('../../../../../../../preview/'+pdf, "", "width=800, height=500,left=300");
	}
*/
	
  }});
});


//CARGA EN EL SELECT LA SUB AREA LABORAL DEPENDIENDO EL DEPARTAMENTO

		$("#list_area_remitente").change(function() {
			$("#list_area_remitente option:selected").each(function() {
				var id_area_laboral = $('#list_area_remitente').val();
				if(id_area_laboral!=1000 && id_area_laboral!="")
				{
					$('#content_sub_area_laboral').empty();
					$('#content_sub_area_laboral').prepend('<img src="../../img/ui-anim_basic_16x16.gif" />');
					$.post("../../int_recibida/reportes/get_sub_area_laboral", {
					id_area_laboral : id_area_laboral
					}, function(data) {
						
						$("#content_sub_area_laboral").html(data);
						$('<option value="0" class="option_sub_area_laboral" selected="selected">Seleccione-Sub Area Laboral</option>').appendTo("#list_sub_area_laboral");
					});
				}
				else
				{
					$('.option_sub_area_laboral').remove();
				 	$('<option value="0" class="option_sub_area_laboral">Seleccione-Sub Area Laboral</option>').appendTo("#list_sub_area_laboral");
				}
			});
		})
		
		
		

		
		//add con copia
		$("#add_con_copia").click(function(){
			//$("#content_con_copia").empty();
			if ($('#list_con_copia').length==0)
			{
				$("#content_con_copia").append('<li><label for="CC">CC</label><div id="content_copia"></div></li>');
				$('select#list_funcionario').clone().attr('id', 'list_con_copia').appendTo('#content_copia');
				$('select#list_con_copia').attr("name","list_con_copia");
			}
			else
			{
				$("#content_con_copia").empty();
			}
		});
		
		
		
		$("#prueba").click(function(){
			if ($('#list_con_copia').length==0)
			{
				var location		=	$('#location').val();
				var id_area_laboral = 	$("[name='list_area_laboral']").val();
				var url				=	'';
				
				$("#content_con_copia").append('<li><label for="CC">CC:</label><select id="list_con_copia" name="list_con_copia"></select></li>');
				
				if(location !=1)
				{
					url="../../int_enviada/int_enviada/get_funcionarios";
				}
				else
				{
					url="../../../../../../../get_funcionarios/";
				}
				
				$.post(url, {
					id_area_laboral : id_area_laboral
				}, function(data) {
						$("#list_con_copia").html(data);
					});
				
			}
			else
			{
				$("#content_con_copia").empty();
			}
		});
		
	
	 //abrir_dialog_correlativo
 $(".a_modal").click(function(){
	 
   	$("#dialog").dialog({
   	modal: true,
   	title: "Cambiar Estado",
   	height:320,
   	width: 481,
   	maxWidth: 600,
   	show: "fold",
   	hide: "scale"
	});
		$("#dialog").empty();
		$("#dialog").html('Por favor, espere...');
		id_corresp= $(this).attr("id");
		$.post("../../int_enviada/int_enviada/modal_status", {
		id_corresp : id_corresp
	}, function(data) {
			
			$("#dialog").html(data);
		});

			
	
	
	
	
  });
  
  


			
		
}); //fin jquery
 
 
 update_corresp= function()
  {
	var id_status=$('#selec_status :selected').val();
	var text=$("#text").val();
	$.post("../../int_enviada/int_enviada/update_status", {
	id_corresp : id_corresp,
	id_status  : id_status,
	text	   : text
	}, function(data) {
			if(data==1)
			{
				$("#info").html("Actualizacion Exitosa");
				
				setTimeout(function() 
				{
					location.reload();
				}, 2000);
				
			}
			else
			{
				
				$("#info").html("Ocurrio un error");
			}
			
			
		});
	}	










//////////////////////////////
	
////
	
if(window.attachEvent){
	window.attachEvent("onload",cargar_eventos,false);
}
else
{
	addEventListener("load",cargar_eventos,false);
}

function cargar_eventos()
{
	var text_obser= document.getElementById('text_obser');
	text_obser.addEventListener("keyup",cuenta_caracteres,false);
}



function cuenta_caracteres(event)
{
	target = (window.event) ? window.event.srcElement /* for IE*/  : event.target
	var tx_char_rest= document.getElementById('tx_char_rest');
	var ta_char= 2000-parseInt(target.value.length);
	tx_char_rest.innerHTML="Caracteres Restantes: "+ta_char;
}


